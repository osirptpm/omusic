const path = require("path");
const fs = require("fs");
module.exports = (route) => {
    route.get("/", (req, res, next) => {
        let cssFiles = getCssFiles(path.join(__dirname, "../public/stylesheets"), "/public");
        res.render("index", {
            cssFiles: cssFiles
        });
    });
};

// css 파일 경로 가져오기
function getCssFiles(p, dir, cssFiles) {
    if (cssFiles === undefined) cssFiles = [];
    dir = path.posix.join(dir, path.basename(p));
    let files = fs.readdirSync(p);
    files.forEach(file => {
        let stat = fs.statSync(path.join(p, file));
        if (stat.isDirectory()) getCssFiles(path.join(p, file), dir, cssFiles);
        else {
            cssFiles.push(path.posix.format({
                root: "/",
                dir: dir,
                base: file
            }));
        }
    });
    return cssFiles;
}