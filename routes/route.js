module.exports = app => {
    const express = require("express");

    const root = express.Router();
    require("./index")(root);

    const ajax = express.Router();
    require("./ajax")(ajax, app);

    app.use("/", root);
    app.use("/ajax", ajax);
};
