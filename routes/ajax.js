module.exports = (route, app) => {
    route.get("/C", (req, res, next) => {
        //res.render("index", { name: "C"});
        res.send({name: "C"});
    });
    route.get("/D", (req, res, next) => {
        res.render("index", { name: "D"});
    });

    const express = require("express");
    const ajaxTest = express.Router();
    require("./ajaxTest")(ajaxTest);
    app.use("/ajax/player", ajaxTest);
};
