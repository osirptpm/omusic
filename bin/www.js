const httpApp = require("../app");
// const httpsApp = require("../app");
const http = require("http");
// const https = require("https");
// const fs = require("fs");

// const options = {
//     key: fs.readFileSync('/etc/letsencrypt/live/osirptpm.ml/privkey.pem'),
//     cert: fs.readFileSync('/etc/letsencrypt/live/osirptpm.ml/cert.pem')
// };

const hostname = "192.168.0.49";
const httpPort = 80;
// const httpsPort = 443;

const httpServer = http.createServer(httpApp);
// const httpsServer = https.createServer(options, httpsApp);

httpServer.listen(httpPort, hostname, () => {
    console.log(`Server running at http://${hostname}:${httpPort}/`);
});
// httpsServer.listen(httpsPort, hostname, () => {
//     console.log(`Server running at https://${hostname}:${httpsPort}/`);
// });