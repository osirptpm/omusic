const express = require("express");
const path = require("path");

const app = express();

app.locals.pretty = true; // 렌더링된 html 소스코드 줄바꿈
app.set("view engine", "pug");
app.set("views", path.join(__dirname, "views"));

// 라우팅
require(path.join(__dirname, "routes/route"))(app);

// 리소스
app.use("/public", express.static(path.join(__dirname, "public"), {dotfiles: "allow"}));

module.exports = app;