import OImageBlur from "./libs/OImageBlur/OImageBlur.js";
import nowPlayInit from "./nowPlay/nowPlay.js";


class OM {
    constructor() {
    }

    //

    run() {
        this._init();
    }

    //

    // OMusic 로드
    _init() {
        let oImageBlur = new OImageBlur();
        oImageBlur.imageSrc = "/public/images/original/technology-music-sound-things.jpg";
        oImageBlur.blur(url => {
            document.getElementsByClassName("background")[0].style.backgroundImage = "url(\"" + url + "\")";
        });
        nowPlayInit();
    }
}

new OM().run();

// new OMusic().run();
// let player = document.createElement("section");
// player.id = "player1";
// document.body.appendChild(player);
// let el = new OElement(player, "/public/application/player");
// console.log(el);
