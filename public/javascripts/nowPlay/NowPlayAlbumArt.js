import {OEqualizer, OEqualizerVisualizer} from "../libs/OAudioPlayer/OEqualizer.js";
import OImageBlur from "../libs/OImageBlur/OImageBlur.js";


class NowPlayAlbumArt {
    /**
     *
     * @param {OAudioPlayer} oAudioPlayer
     */
    constructor(oAudioPlayer) {
        if (oAudioPlayer.constructor.name !== "OAudioPlayer") throw TypeError("Not a OAudioPlayer");
        this.oEqualizer = new OEqualizer(oAudioPlayer);
        this._handle = null;
    }

    /**
     *
     * @return {*}
     * @private
     */
    get _handle() {
        return this.__handle;
    }
    /**
     *
     * @param number
     * @private
     */
    set _handle(number) {
        this.__handle = number;
    }

    // 앨범아트 표시
    /**
     *
     * @param {PictureObject} _picture
     */
    setAlbumArt(_picture) {
        let blob = new Blob([_picture.pictureData], { type: _picture.mimeType });
        let imageUrl = URL.createObjectURL(blob);
        document.getElementById("albumArt").style.backgroundImage = "url(\"" + imageUrl + "\")";
        this._setAlbumArtBlur(imageUrl);
    }

    // 주파수 애니메이션
    startEqVisualizer() {
        if (this._handle !== null) return;
        let canvas = document.getElementById("eqVisualizerCanvas");
        canvas.width = 1000;
        canvas.height = 1000;
        /*canvas.onclick = () => {
            oAudioPlayer.next();
            if (oAudioPlayer.paused) oAudioPlayer.play();
        };*/

        let option = OEqualizerVisualizer.createEVOption();
        option.centerPoint.X = canvas.width / 2;
        option.centerPoint.Y = canvas.height / 2;
        option.insideRadius = 200;
        option.outsideRadius = 300;
        option.linear = false;
        option.startDegree = 140;
        option.clockwise = false;
        option.color = "rgba(255, 255, 255, .1)";
        this._loop(canvas.getContext("2d"), this.oEqualizer, option);
    }
    stopEqVisualizer() {
        cancelAnimationFrame(this._handle);
        this._handle = null;
    }

    // 비공개 메소드

    // 이미지 블러 처리
    /**
     *
     * @param {string} imageUrl
     * @private
     */
    _setAlbumArtBlur(imageUrl) {
        let oImageBlur = new OImageBlur(); // 낭비 발생
        oImageBlur.imageSrc = imageUrl;
        oImageBlur.blur(url => {
            document.getElementById("nowPlayAlbumArt").style.backgroundImage = "url(\"" + url + "\")";
            URL.revokeObjectURL(imageUrl);
        });
    }

    // 주파수 반복 그리기
    /**
     *
     * @param {CanvasRenderingContext2D} canvasRenderingContext2D
     * @param {OEqualizer} oEqualizer
     * @param {_EVOption} evOption
     * @private
     */
    _loop(canvasRenderingContext2D, oEqualizer, evOption) {
        OEqualizerVisualizer.drawCircle(canvasRenderingContext2D, oEqualizer.getAnalyzedByteFrequencyData, evOption);
        this._handle = requestAnimationFrame(this._loop.bind(this, canvasRenderingContext2D, oEqualizer, evOption));
    }
}

export default NowPlayAlbumArt;






/*
let oExtractAudioMetadata = new OExtractAudioMetadata();
let oAudioPlayer = new OAudioPlayer();

oAudioPlayer.addEventListener("play", async ev => {
    if (oAudioPlayer.getCurrentSong.tag === undefined) {
        oAudioPlayer.getCurrentSong.tag = await oExtractAudioMetadata.getTagListFromSrcAsync(oAudioPlayer.getCurrentSong.src);
    }
    if (oAudioPlayer.getCurrentSong.tag === undefined) return false;
    let picFunc = _picture => {
        let blob = new Blob([_picture.pictureData], { type: _picture.mimeType });
        let imageUrl = URL.createObjectURL(blob);
        document.getElementById("albumArt").style.backgroundImage = "url(\"" + imageUrl + "\")";
        let oImageBlur = new OImageBlur(); // 낭비 발생
        oImageBlur.imageSrc = imageUrl;
        oImageBlur.blur(url => {
            document.getElementsByClassName("background")[0].style.backgroundImage = "url(\"" + url + "\")";
            URL.revokeObjectURL(imageUrl);
        });
    };

    oAudioPlayer.getCurrentSong.tag.forEach(tagObject => {
        if (tagObject.tagName === "ID3") {
            tagObject.frames.forEach(frame => {
                if (frame.header.frameID === "APIC") {
                    picFunc(frame.body);
                }
            });
        } else if (tagObject.tagName === "VorbisComment") {
            if (tagObject.picture !== undefined) picFunc(tagObject.picture);
        }
    });
    console.log(oAudioPlayer.getCurrentSong.tag);
});

let canvas = document.getElementById("eqVisualizerCanvas");
canvas.width = 1000;
canvas.height = 1000;
canvas.onclick = () => {
    oAudioPlayer.next();
    if (oAudioPlayer.paused) oAudioPlayer.play();
};
let oEqualizer = new OEqualizer(oAudioPlayer);
let option = OEqualizerVisualizer.createEVOption();
option.centerPoint.X = canvas.width / 2;
option.centerPoint.Y = canvas.height / 2;
option.insideRadius = 200;
option.outsideRadius = 300;
option.linear = false;
option.startDegree = 140;
option.clockwise = false;
option.color = "rgba(255, 255, 255, .1)";
requestAnimationFrame(function loop() {
    OEqualizerVisualizer.drawCircle(canvas.getContext("2d"), oEqualizer.getAnalyzedByteFrequencyData, option);
    requestAnimationFrame(loop);
});
*/