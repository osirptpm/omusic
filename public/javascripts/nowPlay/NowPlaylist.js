import OTrackManager from "../libs/OAudioPlayer/OTrackManager.js";

class NowPlaylist {
    constructor(oAudioPlayer) {
        if (oAudioPlayer.constructor.name !== "OAudioPlayer") throw TypeError("Not a OAudioPlayer");
        this.trackManager = new OTrackManager();
        this.trackManager.setCurrentTrack = this.trackManager.addTrack("Good");
        this.trackManager.onChangeTrack = trackID => {
            if (this.trackManager.getCurrentTrack.id === trackID)
                oAudioPlayer.setTrack = this.trackManager.getSongsFromTrackID(trackID);
        }
    }

    // 로컬 음악 파일 추가
    /**
     *
     * @param {Array.<File> | FileList} files
     * @param {OAudioPlayer} oAudioPlayer
     */
    addLocalFileSongs(files) {
        console.log(files);
        let trackID = this.trackManager.getCurrentTrack.id;
        let songIDs = this.trackManager.addSongToTrackAsFiles(trackID, files);

        console.log(this.trackManager.getSongsFromTrackID(trackID));
        console.log(this.trackManager.getCurrentTrack);
    }
}

export default NowPlaylist;

/*
    let trackManager = new OTrackManager();
    let trackID = trackManager.addTrack("Good");
    trackManager.setCurrentTrack = trackID;

    let uploadToLocal_input = document.getElementById("uploadToLocal_input");
    uploadToLocal_input.onchange = async () => {
        console.log(uploadToLocal_input.files);
        let trackID = trackManager.getCurrentTrack.id;
        let files = uploadToLocal_input.files;
        let songIDs = trackManager.addSongToTrackAsFiles(trackID, files);
        let songs = trackManager.getSongsFromTrackID(trackID);
        //songs.forEach(async song => {
        //    if (song.isBlobURL) song.tag = await oExtractAudioMetadata.getTagListFromFileAsync(song.file);
        //});
        console.log(songs);
        oAudioPlayer.setTrack = songs;
        console.log(trackManager.getCurrentTrack);
    };
 */