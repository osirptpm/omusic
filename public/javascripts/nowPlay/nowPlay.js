import OAudioPlayer from "../libs/OAudioPlayer/OAudioPlayer.js";
import NowPlayControl from "./NowPlayControl.js";
import NowPlaylist from "./NowPlaylist.js";
import NowPlayAlbumArt from "./NowPlayAlbumArt.js";
import OExtractAudioMetadata from "../libs/OExtractAudioMetadata/OExtractAudioMetadata.js";

function nowPlayInit() {
    let oExtractAudioMetadata = new OExtractAudioMetadata();
    let oAudioPlayer = new OAudioPlayer();


    let nowPlayControl = new NowPlayControl(oAudioPlayer);


    let nowPlaylist = new NowPlaylist(oAudioPlayer);


    let uploadToLocal = document.getElementById("uploadToLocal");
    uploadToLocal.onchange = () => {
        nowPlaylist.addLocalFileSongs(uploadToLocal.files);
    };

    let nowPlayAlbumArt = null; // 바로 안 만드는 이유 Chrome - https://developers.google.com/web/updates/2017/09/autoplay-policy-changes#webaudio

    oAudioPlayer.addEventListener("play", async ev => {
        if (nowPlayAlbumArt === null) { nowPlayAlbumArt = new NowPlayAlbumArt(oAudioPlayer); }
        if (oAudioPlayer.getCurrentSong.tag === undefined) {
            oAudioPlayer.getCurrentSong.tag = await oExtractAudioMetadata.getTagListFromSrcAsync(oAudioPlayer.getCurrentSong.src);
        }
        let tags = oAudioPlayer.getCurrentSong.tag;
        if (tags === undefined) return false;
        tags.forEach(tag => {
            let _picture = tag.toObject().picture;
            if (_picture !== undefined) {
                nowPlayAlbumArt.setAlbumArt(_picture);
            }
        });
        nowPlayAlbumArt.startEqVisualizer();
        console.log(tags);
    });
    oAudioPlayer.addEventListener("pause", async ev => {
        nowPlayAlbumArt.stopEqVisualizer();
    });
    oAudioPlayer.addEventListener("ended", async ev => {
        nowPlayAlbumArt.stopEqVisualizer();
    });

}
export default nowPlayInit;