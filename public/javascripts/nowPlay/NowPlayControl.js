import {PPButton, NextButton, PreviousButton} from "../libs/OAudioPlayer/OAudioControlButtons.js";

class NowPlayControl {
    constructor(oAudioPlayer) {
        if(oAudioPlayer.constructor.name !== "OAudioPlayer") throw TypeError("Not a OAudioPlayer");
        this._drawControlButton();
        this._addEventListenerToButton(oAudioPlayer);
    }

    /**
     *
     * @return {PreviousButton}
     */
    get previousButton() {
        return this._previousButton;
    }
    /**
     *
     * @param {PreviousButton} previousButton
     */
    set previousButton(previousButton) {
        if(previousButton.constructor.name !== "PreviousButton") throw TypeError("Not a PreviousButton");
        this._previousButton = previousButton;
    }

    /**
     *
     * @return {PPButton}
     */
    get ppButton() {
        return this._ppButton;
    }
    /**
     *
     * @param {PPButton} ppButton
     */
    set ppButton(ppButton) {
        if(ppButton.constructor.name !== "PPButton") throw TypeError("Not a PPButton");
        this._ppButton = ppButton;
    }

    /**
     *
     * @return {NextButton}
     */
    get nextButton() {
        return this._nextButton;
    }
    /**
     *
     * @param {NextButton} nextButton
     */
    set nextButton(nextButton) {
        if(nextButton.constructor.name !== "NextButton") throw TypeError("Not a NextButton");
        this._nextButton = nextButton;
    }

    // 버튼 그리기
    _drawControlButton() {
        let ja_previous_button = document.getElementById("previous_button");
        let ja_PP_button = document.getElementById("PP_button");
        let ja_next_button = document.getElementById("next_button");

        [ja_previous_button, ja_PP_button, ja_next_button].forEach((canvas, i) => {
            let ctx = canvas.getContext("2d");
            ctx.canvas.width = canvas.scrollWidth;
            ctx.canvas.height = canvas.scrollHeight;
            ctx.lineWidth = 1.5;
            ctx.strokeStyle = "rgba(255, 255, 255, 0.5)";
            switch (i) {
                case 0: this.previousButton = new PreviousButton(ctx); break;
                case 1: this.ppButton = new PPButton(ctx); break;
                case 2: this.nextButton = new NextButton(ctx); break;
            }
        });
    }

    // 버튼에 이벤트 추가
    /**
     *
     * @param {OAudioPlayer} oAudioPlayer
     */
    _addEventListenerToButton(oAudioPlayer) {
        let previousButton = this.previousButton;
        let ppButton = this.ppButton;
        let nextButton = this.nextButton;

        oAudioPlayer.addEventListener("play", () => {
            ppButton.PP(oAudioPlayer.paused);
        });
        oAudioPlayer.addEventListener("pause", () => {
            ppButton.PP(oAudioPlayer.paused);
        });

        let ja_previous_button = previousButton.context.canvas;
        let ja_PP_button = ppButton.context.canvas;
        let ja_next_button = nextButton.context.canvas;

        ja_previous_button.addEventListener("click", () => {
            oAudioPlayer.previous();
        });
        ja_PP_button.addEventListener("click", () => {
            oAudioPlayer.PP();
        });
        ja_next_button.addEventListener("click", () => {
            oAudioPlayer.next();
        });
    }
}

export default NowPlayControl;
