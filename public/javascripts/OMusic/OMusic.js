import {OHashController} from "../libs/OHashController/OHashController.js";
import OAudioPlayer from "../libs/OAudioPlayer/OAudioPlayer.js";
import OTrackManager from "../libs/OAudioPlayer/OTrackManager.js";
import OExtractAudioMetadata from "../libs/OExtractAudioMetadata/OExtractAudioMetadata.js";
import fileLoad from "./fileLoad/fileLoad.js";
import player from "./player/player.js";
import sign from "./sign/sign.js";
import user from "./user/user.js";

class _Mode {
    static get EDIT() {
        return "edit"
    }
    static get LISTENING() {
        return "listening"
    }
}

class _User {
    constructor() {
        this._oAudioPlayer = new OAudioPlayer();
        this._oTrackManager = new OTrackManager();
        this._oExtractAudioMetadata = new OExtractAudioMetadata();
        /*
        id, name, email, profile_picture, mode(edit, listen)
         */
    }

    /**
     *
     * @return {OExtractAudioMetadata}
     */
    get oExtractAudioMetadata() {
        return this._oExtractAudioMetadata;
    }
    /**
     *
     * @return {OTrackManager}
     */
    get oTrackManager() {
        return this._oTrackManager;
    }
    /**
     *
     * @return {OAudioPlayer}
     */
    get oAudioPlayer() {
        return this._oAudioPlayer;
    }

    /**
     *
     * @return {number}
     */
    get id() {
        return this._id;
    }
    /**
     *
     * @param {number} number
     */
    set id(number) {
        if (number.constructor.name !== "Number") throw TypeError("Not a Number");
        this._id = number;
    }
    /**
     *
     * @return {string}
     */
    get name() {
        return this._name;
    }
    /**
     *
     * @param {string} string
     */
    set name(string) {
        if (string.constructor.name !== "String") throw TypeError("Not a String");
        this._name = string;
    }
    /**
     *
     * @return {string}
     */
    get email() {
        return this._email;
    }
    /**
     *
     * @param {string} string
     */
    set email(string) {
        if (string.constructor.name !== "String") throw TypeError("Not a String");
        this._email = string;
    }
    /**
     *
     * @return {string}
     */
    get mode() {
        return this._mode;
    }
    /**
     *
     * @param {string} string
     */
    set mode(string) {
        if (string.constructor.name !== "String") throw TypeError("Not a String");
        this._mode = string;
    }
}

class OMusic {
    constructor() {
        this.__oHashController = new OHashController();
        this.__user = new _User();
        this._initRoute();

        let user = this._user;
        user.id = 0;
        user.name = "Osirp";
        user.email = "osirptpm@gmail.com";
        user.mode = _Mode.LISTENING;
    }

    /**
     *
     * @return {OHashController}
     * @private
     */
    get _oHashController() {
        return this.__oHashController;
    }
    /**
     *
     * @return {_User}
     * @private
     */
    get _user() {
        return this.__user;
    }

    run() {
        let user = this._user;
        console.log(user);

        let ja_uploadToLocal = document.getElementById("uploadToLocal");
        ja_uploadToLocal.onchange = async ev => {
            let files = ja_uploadToLocal.files;
            let oExtractAudioMetadata = new OExtractAudioMetadata();
            let tags;
            for (let i = 0; i < files.length; i++) {
                console.log(files[i]);
                tags = await oExtractAudioMetadata.getTagListFromFileAsync(files[i]);
                console.log(tags);
            }
        }
    }

    _initRoute() {
        let oHashController = this._oHashController;
        let __user = this._user;

        let _fileLoad = new fileLoad(oHashController, __user);
        this._oHashController.addHashEvent("fileLoad", () => _fileLoad.on(), () => _fileLoad.off());
        let _player = new player(oHashController, __user);
        this._oHashController.addHashEvent("player", () => _player.on(), () => _player.off());
        let _sign = new sign(oHashController, __user);
        this._oHashController.addHashEvent("sign", () => _sign.on(), () => _sign.off());
        let _user = new user(oHashController, __user);
        this._oHashController.addHashEvent("user", () => _user.on(), () => _user.off());
    }

}

export default OMusic;