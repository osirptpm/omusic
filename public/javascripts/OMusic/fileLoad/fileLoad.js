class fileLoad {
    constructor(oHashController, _user) {
        this.__oHashController = oHashController;
        this.__user = _user;
    }

    /**
     *
     * @return {OHashController}
     * @private
     */
    get _oHashController() {
        return this.__oHashController;
    }
    /**
     *
     * @return {_User}
     * @private
     */
    get _user() {
        return this.__user;
    }

    on() {
        console.log(this._user);
    }
    off() {
    }
}
export default fileLoad;