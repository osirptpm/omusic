onmessage = (event) => {
    let imageData = event.data.imageData;
    let degree = event.data.degree;
    for (let i = 0; i < degree; i++)
        _blur(imageData.data, imageData.width, imageData.height);
    postMessage(imageData);
};

function _blur(iData, rW, rH) {
    let bW = rW * 4,
        bH = rH;
    let iDataLen = iData.length;

    for (let i = 0; i < iDataLen - 3; i+=4) {
        let rSum = 0, gSum = 0, bSum = 0, aSum = 0;
        let bitArr = [];
        // 중복 계산 피하기
        let UL = i - bW - 4,
            UM = i - bW,
            UR = i - bW + 4,
            ML = i - 4,
            MM = i,
            MR = i + 4,
            BL = i + bW - 4,
            BM = i + bW,
            BR = i + bW + 4;
        // 중복 계산 피하기
        let ibW = i % bW,
            bw4 = bW - 4,
            bWbHbW =  bW * bH - bW;

        if (ibW >= 4 && // 왼쪽
            ibW < bw4 && // 오른쪽
            i >= bW && // 위쪽
            i < bWbHbW) // 아래쪽
        { // 안쪽
            bitArr = [
                UL, UM, UR,
                ML, MM, MR,
                BL, BM, BR
            ];
        }
        // 바깥쪽
        else if (ibW < 4 && i < bW) { // 왼쪽 위
            bitArr = [
                MM, MR,
                BM, BR
            ];
        } else if (ibW >= bw4 && i < bW) { // 오른쪽 위
            bitArr = [
                ML, MM,
                BL, BM
            ];
        } else if (ibW < 4 && i >= bWbHbW) { // 왼쪽 아래
            bitArr = [
                UM, UR,
                MM, MR,
            ];
        } else if (ibW >= bw4 && i >= bWbHbW) { // 오른쪽 아래
            bitArr = [
                UL, UM,
                ML, MM
            ];
        } else if (ibW < 4) { // 왼쪽
            bitArr = [
                UM, UR,
                MM, MR,
                BM, BR
            ];
        } else if (ibW >= bw4) { // 오른쪽
            bitArr = [
                UL, UM,
                ML, MM,
                BL, BM
            ];
        } else if (i < bW) { // 위쪽
            bitArr = [
                ML, MM, MR,
                BL, BM, BR
            ];
        } else if (i >= bWbHbW) { // 아래쪽
            bitArr = [
                UL, UM, UR,
                ML, MM, MR,
            ];
        }

        let bitArrLen = bitArr.length;
        for (let j= 0; j < bitArrLen; j++) {
            rSum += iData[bitArr[j]];
            gSum += iData[bitArr[j] + 1];
            bSum += iData[bitArr[j] + 2];
            aSum += iData[bitArr[j] + 3];
        }
        iData[i] = rSum / bitArrLen;
        iData[i + 1] = gSum / bitArrLen;
        iData[i + 2] = bSum / bitArrLen;
        iData[i + 3] = aSum / bitArrLen;
    }
}