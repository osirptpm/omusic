class _BlurredImageInfo {
    constructor() {
        this.rate = 100;
        for (let i = 0; i < arguments.length; i++) {
            if (arguments[i].constructor.name === "Number") this.rate = arguments[i];
            if (arguments[i].constructor.name === "Blob") this.blob = arguments[i];
            if (arguments[i].constructor.name === "String") this.src = arguments[i];
        }
    }
    /**
     *
     * @return {number}
     */
    get degree() {
        return this.__degree;
    }
    /**
     *
     * @param {number} number
     */
    set _degree(number) {
        if (number.constructor.name !== "Number") throw TypeError("Not a Number");
        this.__degree = number;
    }

    /**
     *
     * @return {number}
     */
    get quality() {
        return this.__quality;
    }
    /**
     *
     * @param {number} number
     */
    set _quality(number) {
        if (number.constructor.name !== "Number") throw TypeError("Not a Number");
        this.__quality = number;
    }

    /**
     *
     * @return {number}
     */
    get rate() {
        return this._rate;
    }
    /**
     *
     * @param {number} number
     */
    set rate(number) {
        if (number.constructor.name !== "Number") throw TypeError("Not a Number");
        if (number < 0 || number > 100) throw RangeError("Between 0 and 100");
        this._rate = number;
        // rate 로 블러정도 결정
        this._degree = (number !== 0) ? Math.round(1 / 11 * number + 1) : 0;
        this._quality = number;
    }

    /**
     *
     * @return {Blob}
     */
    get blob() {
        return this._blob;
    }
    /**
     *
     * @param {Blob} blob
     */
    set blob(blob) {
        if (blob.constructor.name !== "Blob") throw TypeError("Not a blob");
        this._blob = blob;
        this._blobURL = URL.createObjectURL(this.blob);
    }

    /**
     *
     * @return {string}
     */
    get blobURL() {
        return this.__blobURL;
    }
    /**
     *
     * @param {string} string
     */
    set _blobURL(string) {
        if (string.constructor.name !== "String") throw TypeError("Not a String");
        this.__blobURL = string;
    }

    /**
     *
     * @return {string}
     */
    get src() {
        return this._src;
    }
    /**
     *
     * @param {string} string
     */
    set src(string) {
        if (string.constructor.name !== "String") throw TypeError("Not a String");
        this._src = string;
    }
}

class _BlurredInfo extends Array {
    constructor() {
        super();
    }
}

export default class OImageBlur {
    constructor(src, rate) {
        // 속성 초기화
        this._blurredInfo = new _BlurredInfo();
        this.rate = 100; // 기본값
        for (let i = 0; i < arguments.length; i++) {
            switch (typeof arguments[i]) {
                case "string": this.imageSrc = arguments[i]; break;
                case "number": this.rate = arguments[i]; break;
            }
        }
        if (typeof Worker === "undefined") throw Error("Worker API를 지원하지 않는 브라우저입니다.");
        this._worker = new Worker("/public/javascripts/libs/OImageBlur/OIB_worker.js");
    }

    // 비공개 속성
    get _worker() { return this.__worker; }
    set _worker(worker) {
        if (typeof worker === "object") {
            this.__worker = worker;
        } else throw TypeError("Object가 아닙니다.");
    }
    /**
     *
     * @return {_BlurredInfo}
     */
    get _blurredInfo() {
        return this.__blurredInfo;
    }
    /**
     *
     * @param {_BlurredInfo} _blurredInfo
     */
    set _blurredInfo(_blurredInfo) {
        if (_blurredInfo.constructor.name !== "_BlurredInfo") throw TypeError("Not a _BlurredInfo");
        this.__blurredInfo = _blurredInfo;
    }

    //공개 속성
    get imageSrc() { return this.__imageSrc; }
    set imageSrc(string) {
        if (string.constructor.name !== "String") throw URIError("주소를 입력해주세요.");
        this.__imageSrc = string;
    }
    get rate() { return this.__rate; }
    set rate(rate) {
        rate = parseInt(rate);
        if (!isNaN(rate)) {
            if (rate >= 0 && rate <= 100) {
                this.__rate = rate;
            }
            else throw RangeError("0 ~ 100사이의 값을 입력해주세요.");
        } else throw TypeError("흐림 정도 값을 숫자로 입력해주세요.");
    }

    // 사용자 호출 메소드 blob 주소값 보내줌
    blur(callback) {
        this._blur().then(resolve => {
            callback(resolve);
        }, reason => {
            throw reason;
        });
    }
    // 모든 URL revokeObjectURL
    clear() {
        this._blurredInfo.forEach(_blurredImageInfo => {
            URL.revokeObjectURL(_blurredImageInfo.src);
        });
    }

    // 블러처리된 이미지 blob 주소 얻기
    async _blur() {
        if (this.imageSrc === undefined) throw URIError("주소를 입력해주세요.");
        if (this._blurredInfo[0] === undefined || this._blurredInfo[0].src !== this.imageSrc) {
            let blob = await this._requestAndSetImageBlob(this.imageSrc);
            // 이미지 캐시작업
            this._blurredInfo[0] = new _BlurredImageInfo(0, blob, this.imageSrc); // 원본 이미지 rate 0으로 저장
        }
        return await this._getBlurredImage(this.rate, this._blurredInfo);
    }

    // XMLHttpRequest로 원본 이미지 가져오기 Promise 사용
    _requestAndSetImageBlob(imageSrc) {
        return new Promise((resolve, reject) => {
            let req = new XMLHttpRequest();
            req.open("GET", imageSrc);
            req.setRequestHeader("x-oib-js-ajax", "test"); //todo 서버 헤더 확인 테스트
            req.responseType = "blob";
            req.onload = () => {
                if (req.status === 200) {
                    resolve(req.response);
                }
                else {
                    reject(Error(req.statusText));
                }
            };
            req.onerror = () => {
                reject(Error("Network Error"));
            };
            req.send();
        });
    }

    // 이미지 블러 처리
    async _getBlurredImage(rate, blurredInfo) {
        // 이전에 해당 rate로 블러처리한 이미지가 없으면
        if (blurredInfo[rate] === undefined || blurredInfo[rate].src !== blurredInfo[0].src) { // 포문으로 이전 블러 이미지 찾아서 거기서부터 블러처리 (최적화) 작업 불가 (quality 가 달라져서)
            let imageBitmap = await createImageBitmap(blurredInfo[0].blob);
            let _blurredImageInfo = new _BlurredImageInfo(rate, blurredInfo[0].src);
            let resizedImageData = this._getResizedImage(imageBitmap, _blurredImageInfo.quality);
            let blurredImageData = await this._callWorkerForImageBlur(resizedImageData, _blurredImageInfo.degree);
            _blurredImageInfo.blob = await this._getImageDataToBlob(blurredImageData);
            // 이미지 캐시작업
            blurredInfo[rate] = _blurredImageInfo;
        }
        return blurredInfo[rate].blobURL;
    }

    // 이미지 크기 조정 - 퍼포먼스를 위함
    _getResizedImage(imageBitmap, quality) {
        let resize = this._calculateSize(imageBitmap.width, imageBitmap.height, quality);
        return this._resizeImage(imageBitmap, resize);
    }

    // 가로, 세로 중 더 긴 길이를 기준으로 크기 계산
    _calculateSize(originalWidth, originalHeight, quality) {
        let resize = { width: originalWidth, height: originalHeight };
        let expression = -5 * quality + 600;
        if (Math.max(originalWidth, originalHeight) <= expression) return resize;
        if (originalWidth > originalHeight) {
            resize.width = expression;
            resize.height = Math.round(originalHeight * resize.width / originalWidth);
        } else {
            resize.height= expression;
            resize.width = Math.round(originalWidth * resize.height / originalHeight);
        }
        return resize;
    }

    // 캔버스로 크기 조정
    _resizeImage(imageBitmap, resize) {
        let canvas = document.createElement("canvas");
        canvas.width = resize.width;
        canvas.height = resize.height;
        let ctx = canvas.getContext("2d");
        ctx.drawImage(imageBitmap, 0, 0, resize.width, resize.height);
        return ctx.getImageData(0, 0, resize.width, resize.height);
    }

    // 이미지 블러 처리 Worker 이용
    _callWorkerForImageBlur(imageData, degree) {
        return new Promise((resolve, reject) => {
            this._worker.onmessage = (event) => {
                resolve(event.data);
                this._worker.terminate();
            };
            this._worker.onerror = (event) => {
                reject(event);
                this._worker.terminate();
            };
            this._worker.postMessage({
                imageData: imageData,
                degree: degree
            });
        });
    }

    // imageData를 blob 값으로 리턴
    _getImageDataToBlob(blurredImageData) {
        return new Promise(((resolve) => {
            let canvas = document.createElement("canvas");
            canvas.width = blurredImageData.width;
            canvas.height = blurredImageData.height;
            let ctx = canvas.getContext("2d");
            ctx.putImageData(blurredImageData, 0, 0, 0, 0, blurredImageData.width, blurredImageData.height);
            canvas.toBlob(blob => {
                resolve(blob);
            });
        }));
    }
}