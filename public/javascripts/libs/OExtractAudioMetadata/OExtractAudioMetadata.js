import OEAM_ID3 from "./OEAM_ID3.js";
import OEAM_VorbisComment from "./OEAM_VorbisComment.js";

class TagList extends Array {
    constructor() {
        super();
    }
    push(tagObject) {
        if (tagObject.constructor.__proto__.name !== "TagObject") throw TypeError("Not a TagObject");
        super.push(tagObject);
    }
}

class OExtractAudioMetadata {
    constructor() {
        this.__defaultCharset = undefined;
    }

    // 비공개 속성

    // 공개 속성
    /**
     *
     * @return {*}
     */
    get getDefaultCharset() {
        return this.__defaultCharset;
    }
    /**
     *
     * @param {String} charset
     */
    set setDefaultCharset(charset) {
        this.__defaultCharset = charset;
    }

    // 사용자 호출 메소드
    /**
     *
     * @param {File} file
     * @return {Promise<TagList>}
     */
    async getTagListFromFileAsync(file) {
        let uInt8Array = await this._getUint8ArrayFromFile(file);
        return await this._getTagListAsync(uInt8Array);
    }
    /**
     *
     * @param {String} src
     * @return {Promise<TagList>}
     */
    async getTagListFromSrcAsync(src) {
        let uInt8Array = await this._getUint8ArrayFromSrc(src);
        return await this._getTagListAsync(uInt8Array);
    }

    // 비공개 메소드

    // Uint8Array로 TagList 가져오기
    /**
     *
     * @param {Uint8Array} uInt8Array
     * @return {Promise<TagList>}
     * @private
     */
    async _getTagListAsync(uInt8Array) {
        /*let textEncoder = new TextDecoder();
        let text = textEncoder.decode(uInt8Array.subarray(0, 5000));
        console.log(text);*/
        let tagList = new TagList();
        let vC = await this._getVorbisCommentAsync(uInt8Array);
        if (vC !== undefined) tagList.push(vC);
        let id3s = await this._getID3Async(uInt8Array);
        if (id3s !== undefined) id3s.forEach(id3 => tagList.push(id3));
        if (tagList.length === 0) tagList = undefined;
        return tagList;
    }

    // Vorbis Comment 가져오기
    /**
     *
     * @param {Uint8Array} uInt8Array
     * @return {Promise<_VorbisComment>}
     * @private
     */
    async _getVorbisCommentAsync(uInt8Array) {
        let o_VorbisComment = new OEAM_VorbisComment();
        return o_VorbisComment.getVorbisComment(uInt8Array);
    }

    // ID3 태그 가져오기
    /**
     *
     * @param {Uint8Array} uInt8Array
     * @return {Promise<Array>}
     * @private
     */
    async _getID3Async(uInt8Array) {
        let o_ID3 = new OEAM_ID3();
        o_ID3.setDefaultCharset = this.getDefaultCharset;
        return o_ID3.getID3(uInt8Array);
    }

    // File 에서 ArrayBuffer 가져오기
    /**
     *
     * @param {File} file
     * @return {Promise<Uint8Array>}
     * @private
     */
    _getUint8ArrayFromFile(file) {
        return new Promise(resolve => {
            let fileReader = new FileReader();
            fileReader.readAsArrayBuffer(file);
            fileReader.onload = ev => {
                resolve(new Uint8Array(ev.currentTarget.result));
            };
        });
    }
    // XMLHTTP 요청으로 ArrayBuffer 가져오기
    /**
     *
     * @param {string} src
     * @return {Promise<Uint8Array>}
     * @private
     */
    _getUint8ArrayFromSrc(src) {
        return new Promise((resolve, reject) => {
            let req = new XMLHttpRequest();
            req.responseType = "arraybuffer";
            req.onload = function() {
                if (req.status === 200) {
                    resolve(new Uint8Array(req.response));
                }
                else {
                    reject(Error(req.statusText));
                }
            };
            req.open("GET", src);
            req.send();
        });
    }
}

export default OExtractAudioMetadata;