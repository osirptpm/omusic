import {TagObject, PictureObject} from "./ObjectInterfaces.js";

class _BlockType {
    static get VORBISCOMMENT() { return 4 }
    static get PICTURE() { return 6 }
}
class _FileFormat {
    static get FLAC() { return "fLaC" }
    static get OGG() { return "OggS" }
}

class _Comment {
    constructor() {
        /*let comment = {
            "TITLE": undefined,
            "VERSION": undefined,
            "ALBUM": undefined,
            "TRACKNUMBER": undefined,
            "ARTIST": undefined,
            "PERFORMER": undefined,
            "COPYRIGHT": undefined,
            "LICENSE": undefined,
            "ORGANIZATION": undefined,
            "DESCRIPTION": undefined,
            "GENRE": undefined,
            "DATE": undefined,
            "LOCATIONd": undefined,
            "CONTACT": undefined,
            "ISRC": undefined
        };*/
    }
}
/**
 * @implements {PictureObject}
 */
class _Picture extends PictureObject{
    constructor() {
        super();
        this.__pictureTypeArray = [
            "Other",
            "32x32 pixels 'file icon' (PNG only)",
            "Other file icon",
            "Cover (front)",
            "Cover (back)",
            "Leaflet page",
            "Media (e.g. label side of CD)",
            "Lead artist/lead performer/soloist",
            "Artist/performer",
            "Conductor",
            "Band/Orchestra",
            "Composer",
            "Lyricist/text writer",
            "Recording Location",
            "During recording",
            "During performance",
            "Movie/video screen capture",
            "A bright coloured fish",
            "Illustration",
            "Band/artist logotype",
            "Publisher/Studio logotype"
        ];
    }

    // 비공개 속성
    /**
     *
     * @return {string[]}
     * @private
     */
    get _pictureTypeArray() {
        return this.__pictureTypeArray;
    }

    // 공개 속성
    get pictureType() {
        return this._pictureType;
    }
    set pictureType(number) {
        if (number.constructor.name !== "Number") throw TypeError("Not a Number");
        this._pictureType = this._pictureTypeArray[number];
    }
    get mimeType() {
        return this._mimeType;
    }
    set mimeType(string) {
        if (string.constructor.name !== "String") throw TypeError("Not a String");
        this._mimeType = string;
    }
    get description() {
        return this._description;
    }
    set description(string) {
        if (string.constructor.name !== "String") throw TypeError("Not a String");
        this._description = string;
    }
    get width() {
        return this._width;
    }
    set width(number) {
        if (number.constructor.name !== "Number") throw TypeError("Not a Number");
        this._width = number;
    }
    get height() {
        return this._height;
    }
    set height(number) {
        if (number.constructor.name !== "Number") throw TypeError("Not a Number");
        this._height = number;
    }
    get colorDepth() {
        return this._colorDepth;
    }
    set colorDepth(number) {
        if (number.constructor.name !== "Number") throw TypeError("Not a Number");
        this._colorDepth = number;
    }
    get numberOfColorsUsed() {
        return this._numberOfColorsUsed;
    }
    set numberOfColorsUsed(number) {
        if (number.constructor.name !== "Number") throw TypeError("Not a Number");
        this._numberOfColorsUsed = number;
    }
    get pictureData() {
        return this._pictureData;
    }
    set pictureData(Array) {
        if (Array.constructor.__proto__.name !== "ArrayBuffer" && Array.constructor.__proto__.name !== "TypedArray") throw TypeError("Not an ArrayBuffer or TypedArray");
        this._pictureData = Array;
    }
}

/**
 * @implements {TagObject}
 */
class _VorbisComment extends TagObject {
    constructor() {
        super();
        super.tagName = "VorbisComment";
        this.vendor = "";
        this.comment = new _Comment();
    }

    /**
     *
     * @return {string}
     */
    get tagName() {
        return this.__tagName;
    }
    /**
     *
     * @param {string} string
     */
    set tagName(string) {
        if (string.constructor.name !== "String") throw TypeError("Not a String");
        this.__tagName = string;
    }
    /**
     *
     * @return {string}
     */
    get vendor() {
        return this._vendor;
    }
    /**
     *
     * @param {string} string
     */
    set vendor(string) {
        if (string.constructor.name !== "String") throw TypeError("Not a String");
        this._vendor = string;
    }
    /**
     *
     * @return {_Comment}
     */
    get comment() {
        return this._comment;
    }
    /**
     *
     * @param {_Comment} _comment
     */
    set comment(_comment) {
        if (_comment.constructor.name !== "_Comment") throw TypeError("Not a _Comment");
        this._comment = _comment;
    }
    /**
     *
     * @return {_Picture}
     */
    get picture() {
        return this.__picture;
    }
    /**
     *
     * @param {_Picture} _picture
     */
    set picture(_picture) {
        if (_picture.constructor.name !== "_Picture") throw TypeError("Not a _Picture");
        this.__picture = _picture;
    }

    toObject() {
        // todo TagObject toObject
        return {
            picture: this.picture
        }
    }
}

class OExtract_VorbisComment {
    constructor() {
    }

    // 비공개 속성

    // 공개 속성

    // 사용자 호출 메소드
    /**
     *
     * @param {Uint8Array} uInt8Array
     * @return {Promise<_VorbisComment>}
     */
    async getVorbisComment(uInt8Array) {
        let vorbisComment = new _VorbisComment();

        switch (this._checkFileFormat(uInt8Array)) {
            case _FileFormat.FLAC: {
                let blocks = this._getBlockIndexFromFLAC(uInt8Array);
                blocks.forEach(block => {
                    switch (block.type) {
                        case _BlockType.VORBISCOMMENT: {
                            let vC = this._getVorbisComment(block.uInt8Array);
                            vorbisComment.vendor = vC.vendor;
                            vorbisComment.comment = vC.comment;
                            break;
                        }
                        case _BlockType.PICTURE: {
                            vorbisComment.picture = this._getPictureFromFLAC(block.uInt8Array);
                            break;
                        }
                    }
                });
                if (blocks.length === 0) vorbisComment = undefined;
                break;
            }
            case _FileFormat.OGG: {
                //todo vorbisComment OGG 확장자
                console.log(_FileFormat.OGG);
                break;
            }
            default: vorbisComment = undefined; break;
        }
        return vorbisComment;
    }


    // 비공개 메소드
    /**
     *
     * @param {Uint8Array} uInt8Array
     * @return {string}
     * @private
     */
    _checkFileFormat(uInt8Array) {
        return String.fromCharCode(...uInt8Array.subarray(0, 4));
    }

    // VorbisComment, Picture 블록 가져오기
    /**
     *
     * @param {Uint8Array} uInt8Array
     * @return {Array.<{ type: number, uInt8Array: Uint8Array }>}
     * @private
     */
    _getBlockIndexFromFLAC(uInt8Array) {
        let blocks = [];

        let index = 4;
        while (index < uInt8Array.length) {
            let isLast = uInt8Array[index] >> 7;
            let blockType = uInt8Array[index] & 127;
            index += 1;
            // index = 5

            let length = this._getIntFromUint8Array(uInt8Array.subarray(index, index + 3), false, false);
            index += 3;
            // index = 8

            if (blockType === 4 || blockType === 6) { // 사진이 여러개 가능
                let block = {
                    type: blockType,
                    uInt8Array: uInt8Array.subarray(index, index + length)
                };
                blocks.push(block);
            }

            index += length;
            if (isLast === 1) break;
        }
        return blocks;
    }


    // VorbisComment 가져오기
    /**
     *
     * @param {Uint8Array} uInt8Array
     * @return {_VorbisComment}
     * @private
     */
    _getVorbisComment(uInt8Array) {
        let vorbisComment = new _VorbisComment();

        let index = 0;
        let vendorLength = this._getIntFromUint8Array(uInt8Array.subarray(index, index + 4), false, true);
        index += 4;

        let decoder = new TextDecoder("UTF-8");
        let vendorString = decoder.decode(uInt8Array.subarray(index, index + vendorLength));
        index += vendorLength;
        vorbisComment.vendor = vendorString;

        let commentListLength = this._getIntFromUint8Array(uInt8Array.subarray(index, index + 4), false, true);
        index += 4;

        for (let i = 0; i < commentListLength; i++) {
            let commentLength = this._getIntFromUint8Array(uInt8Array.subarray(index, index + 4), false, true);
            index += 4;

            let commentString = decoder.decode(uInt8Array.subarray(index, index + commentLength));
            index += commentLength;

            let commentStringArray = commentString.split("=");
            vorbisComment.comment[commentStringArray[0].toUpperCase()] = commentStringArray[1];
        }
        //let framing_bit = uInt8Array[index]; ???
        return vorbisComment;
    }

    // flac에서 picture 가져오기
    /**
     *
     * @param {Uint8Array} uInt8Array
     * @return {_Picture}
     * @private
     */
    _getPictureFromFLAC(uInt8Array) {
        let picture = new _Picture();
        let index = 0;
        let decoder = new TextDecoder("UTF-8");
        picture.pictureType = this._getIntFromUint8Array(uInt8Array.subarray(index, index += 4), false, false);
        let mimeTypeLength = this._getIntFromUint8Array(uInt8Array.subarray(index, index += 4), false, false);
        picture.mimeType = String.fromCharCode(...uInt8Array.subarray(index, index += mimeTypeLength));
        let descriptionLength = this._getIntFromUint8Array(uInt8Array.subarray(index, index += 4), false, false);
        picture.description = decoder.decode(uInt8Array.subarray(index, index += descriptionLength));
        picture.width = this._getIntFromUint8Array(uInt8Array.subarray(index, index += 4), false, false);
        picture.height = this._getIntFromUint8Array(uInt8Array.subarray(index, index += 4), false, false);
        picture.colorDepth = this._getIntFromUint8Array(uInt8Array.subarray(index, index += 4), false, false);
        picture.numberOfColorsUsed = this._getIntFromUint8Array(uInt8Array.subarray(index, index += 4), false, false);
        let pictureDataLength = this._getIntFromUint8Array(uInt8Array.subarray(index, index += 4), false, false);
        picture.pictureData = uInt8Array.subarray(index, index + pictureDataLength);
        return picture;
    }

    // Uint8Array 바이트로 정수 구하기, 동기 안정 정수인지, 빅에디안 인지
    /**
     *
     * @param {Uint32Array} uInt8Array
     * @param {Boolean} isSynchsafe
     * @param {Boolean} isBigEndian
     * @return {number}
     * @private
     */
    _getIntFromUint8Array(uInt8Array, isSynchsafe, isBigEndian) {
        let number = 0;
        let cal = (j) => {
            let k = (isSynchsafe) ? 7 : 8; // 0xxx xxxx : xxxx xxxx
            return (isBigEndian) ? (j * k) : ((uInt8Array.length - 1) * k) - (k * j); // 앞 바이트가 작은수
        };
        for (let i = 0; i < uInt8Array.length; i++) {
            number += uInt8Array[i] << cal(i);
        }
        return number;
    }

}

export default OExtract_VorbisComment;