import {TagObject, PictureObject} from "./ObjectInterfaces.js";

/**
 * @implements {PictureObject}
 */
class _Picture extends PictureObject {
    constructor() {
        super();
        this.__pictureTypeArray = [
            "Other",
            "32x32 pixels 'file icon' (PNG only)",
            "Other file icon",
            "Cover (front)",
            "Cover (back)",
            "Leaflet page",
            "Media (e.g. label side of CD)",
            "Lead artist/lead performer/soloist",
            "Artist/performer",
            "Conductor",
            "Band/Orchestra",
            "Composer",
            "Lyricist/text writer",
            "Recording Location",
            "During recording",
            "During performance",
            "Movie/video screen capture",
            "A bright coloured fish",
            "Illustration",
            "Band/artist logotype",
            "Publisher/Studio logotype"
        ];
    }

    // 비공개 속성
    /**
     *
     * @return {string[]}
     * @private
     */
    get _pictureTypeArray() {
        return this.__pictureTypeArray;
    }

    // 공개 속성
    get textEncoding() {
        return this._textEncoding;
    }
    set textEncoding(string) {
        if (string.constructor.name !== "String") throw TypeError("Not a String");
        this._textEncoding = string;
    }
    get pictureType() {
        return this._pictureType;
    }
    set pictureType(number) {
        if (number.constructor.name !== "Number") throw TypeError("Not a Number");
        this._pictureType = this._pictureTypeArray[number];
    }
    get mimeType() {
        return this._mimeType;
    }
    set mimeType(string) {
        if (string.constructor.name !== "String") throw TypeError("Not a String");
        this._mimeType = string;
    }
    get description() {
        return this._description;
    }
    set description(string) {
        if (string.constructor.name !== "String") throw TypeError("Not a String");
        this._description = string;
    }
    get pictureData() {
        return this._pictureData;
    }
    set pictureData(Array) {
        if (Array.constructor.__proto__.name !== "ArrayBuffer" && Array.constructor.__proto__.name !== "TypedArray") throw TypeError("Not an ArrayBuffer or TypedArray");
        this._pictureData = Array;
    }
}

/**
 * @implements {TagObject}
 */
class _ID3 extends TagObject {
    constructor() {
        super();
        super.tagName = "ID3";
        this._header = undefined;
        this._frames = undefined;
    }
    get tagName() {
        return this.__tagName;
    }
    set tagName(string) {
        if (string.constructor.name !== "String") throw TypeError("Not a String");
        this.__tagName = string;
    }
    get header() {
        return this._header;
    }
    set header(object) {
        if (object.constructor.name !== "Object") throw TypeError("Not a Object");
        this._header = object;
    }
    get frames() {
        return this._frames;
    }
    set frames(array) {
        if (array.constructor.name !== "Array") throw TypeError("Not a Array");
        this._frames = array;
    }

    toObject() {
        // todo ITagObject toObject
        let obj = {
            picture: undefined
        };
        this.frames.forEach(frame => {
            if (frame.header.frameID === "APIC") {
                obj.picture = frame.body;
            }
        });
        return obj;
    }

}

class OExtractID3 {
    constructor() {
        this.__defaultCharset = undefined;

        // todo 프레임 명세 추가
        this.__frameIDFuncs1 = {
            // v2.2
            TXX: v => this._getFrameOfUserDefinedTextInformation(v),
            WXX: v => this._getFrameOfUserDefinedURLLink(v),
            COM: v => this._getFrameOfComments(v),
            PIC: v => this._getFrameOfAttachedPicture(v, true), // 버전 구분
            ULT: v => this._getFrameOfUnsynchronisedLyricsTranscription(v),
            UFI: v => this._getFrameOfUniqueFileIdentifier(v),
            SLT: v => this._getFrameOfUserDefinedTextInformation(v),
            POP: v => this._getFrameOfPopularimeter(v),
            CNT: v => this._getFrameOfPlayCounter(v),
            GEO: v => this._getFrameOfGeneralEncapsulatedObject(v),

            // v2.3 ~ 2.4
            TXXX: v => this._getFrameOfUserDefinedTextInformation(v),
            WXXX: v => this._getFrameOfUserDefinedURLLink(v),
            COMM: v => this._getFrameOfComments(v),
            APIC: v => this._getFrameOfAttachedPicture(v, false), // 버전 구분
            USLT: v => this._getFrameOfUnsynchronisedLyricsTranscription(v),
            PRIV: v => this._getFrameOfPrivate(v),
            UFID: v => this._getFrameOfUniqueFileIdentifier(v),
            SYLT: v => this._getFrameOfSynchronisedLyrics(v),
            MCDI: v => this._getFrameOfMusicCDIdentifier(v),
            POPM: v => this._getFrameOfPopularimeter(v),
            PCNT: v => this._getFrameOfPlayCounter(v),
            GEOB: v => this._getFrameOfGeneralEncapsulatedObject(v),
            RVAD: v => this._getFrameOfRelativeVolumeAdjustment(v)
        };
        this.__frameIDFuncs2 = {
            // v2.1 ~ 2.4
            T: v => this._getFrameOfTextInformation(v),
            W: v => this._getFrameOfURLLink(v),
        };
        this.setFastMode = true;
    }

    // 비공개 속성
    /**
     *
     * @return {{TXX: function(*=), WXX: function(*=), COM: function(*=), PIC: function(*=), ULT: function(*=), UFI: function(*=), SLT: function(*=), POP: function(*=), CNT: function(*=), GEO: function(*=), TXXX: function(*=), WXXX: function(*=), COMM: function(*=), APIC: function(*=), USLT: function(*=), PRIV: function(*=), UFID: function(*=), SYLT: function(*=), MCDI: function(*=), POPM: function(*=), PCNT: function(*=), GEOB: function(*=), RVAD: function(*=)}|*}
     * @private
     */
    get _getFrameIDFuncs1() {
        return this.__frameIDFuncs1;
    }
    /**
     *
     * @return {{T: function(*=), W: function(*=)}|*}
     * @private
     */
    get _getFrameIDFuncs2() {
        return this.__frameIDFuncs2;
    }

    // 공개 속성
    /**
     *
     * @return {*}
     */
    get getDefaultCharset() {
        return this.__defaultCharset;
    }
    /**
     *
     * @param {String} charset
     */
    set setDefaultCharset(charset) {
        this.__defaultCharset = charset;
    }

    /**
     *
     * @return {Boolean}
     */
    get getFastMode() {
        return this.__fastMode;
    }
    /**
     *
     * @param {Boolean} boolean
     */
    set setFastMode(boolean) {
        this.__fastMode = boolean;
    }

    // 사용자 호출 메소드
    // ID3 태그 가져오기
    /**
     *
     * @param {Uint8Array} arrayBuffer
     * @return {Array} ID3s
     */
    getID3(arrayBuffer) {
        /*let ID3 = {
            header: undefined,
            frames: undefined
        };*/
        let ID3s = [];
        let v1 = (this.getFastMode) ? this._getID3V1IndexArrayFast(arrayBuffer) : this._getID3V1IndexArray(arrayBuffer);
        v1.forEach((v, i, o) => {
            if (v === arrayBuffer.length - 128 || v === 0) {
                ID3s.push(this._getID3V1(arrayBuffer.subarray(v, v + 128)));
            }
        });
        let v2 = (this.getFastMode) ? this._getID3V2IndexArrayFast(arrayBuffer) : this._getID3V2IndexArray(arrayBuffer);
        v2.forEach((v, i, o) => {
            ID3s.push(this._getID3V2(arrayBuffer.subarray(v, arrayBuffer.length)));
        });
        //console.log(ID3s);
        if (ID3s.length === 0) ID3s = undefined;
        return ID3s;
    }

    // 비공개 메소드

    // 비트 패턴으로 태그 인덱스 찾기
    /**
     *
     * @param {Uint8Array} arrayBuffer
     * @return {Array.<number>}
     * @private
     */
    _getID3V1IndexArray(arrayBuffer) {
        let isMatchFunctions = [
            (v) => {
                return v === 84; // T
            },
            (v) => {
                return v === 65; // A
            },
            (v) => {
                return v === 71; // G
            }
        ];
        let indexArray = [];
        for (let i = 0, j = 0, tmp = -1; i < arrayBuffer.length; i++) {
            if (j < isMatchFunctions.length && isMatchFunctions[j](arrayBuffer[i]) && (tmp === -1 || tmp === i - 1)) {
                tmp = i; j++;
            } else { j = 0; tmp = -1; }
            if (j >= isMatchFunctions.length) {
                indexArray.push(i - (isMatchFunctions.length - 1)); j = 0; tmp = -1;
            }
        }
        //console.log(indexArray);
        return indexArray;
    }
    /**
     *
     * @param {Uint8Array} arrayBuffer
     * @return {Array.<number>}
     * @private
     */
    _getID3V2IndexArray(arrayBuffer) {
        let isMatchFunctions = [
            (v) => {
                return v === 73; // I
            },
            (v) => {
                return v === 68; // D
            },
            (v) => {
                return v === 51; // 3
            },
            (v) => {
                return ((v === 2) || (v === 3) || (v === 4)); // 명세는 v < 255, 메이저 버전
            },
            (v) => {
                return v === 0; // 명세는 v < 255, 마이너 버전
            },
            (v) => {
                return (v & 0x0f) === 0; // flag 인지 % xxxx 0000
            },
            (v) => {
                return v < 128; // 안전비트 사용 중 인지 % 0xxx xxxx
            },
            (v) => {
                return v < 128; // 안전비트 사용 중 인지 % 0xxx xxxx
            },
            (v) => {
                return v < 128; // 안전비트 사용 중 인지 % 0xxx xxxx
            },
            (v) => {
                return v < 128; // 안전비트 사용 중 인지 % 0xxx xxxx
            }
        ];
        let indexArray = [];
        for (let i = 0, j = 0, tmp = -1; i < arrayBuffer.length; i++) {
            if (j < isMatchFunctions.length && isMatchFunctions[j](arrayBuffer[i]) && (tmp === -1 || tmp === i - 1)) {
                tmp = i; j++;
            } else { j = 0; tmp = -1; }
            if (j >= isMatchFunctions.length) { indexArray.push(i - (isMatchFunctions.length - 1)); j = 0; tmp = -1; }
        }
        //console.log(indexArray);
        return indexArray;
    }
    // 앞, 뒤만 확인
    /**
     *
     * @param {Uint8Array} arrayBuffer
     * @return {Array.<number>}
     * @private
     */
    _getID3V1IndexArrayFast(arrayBuffer) {
        let isMatchFunctions = [
            (v) => {
                return v === 84; // T
            },
            (v) => {
                return v === 65; // A
            },
            (v) => {
                return v === 71; // G
            }
        ];
        let indexArray = [];
        let boolean = true;

        for (let i = 0; i < isMatchFunctions.length && boolean; i++) {
            boolean = isMatchFunctions[i](arrayBuffer[i]) && boolean;
        }
        if (boolean) indexArray.push(0);
        else boolean = true;

        let endArrayBuffer = arrayBuffer.subarray(arrayBuffer.length - 128, arrayBuffer.length);
        for (let i = 0; i < isMatchFunctions.length && boolean; i++) {
            boolean = isMatchFunctions[i](endArrayBuffer[i]) && boolean;
        }
        if (boolean) indexArray.push(arrayBuffer.length - 128);
        //console.log(indexArray);
        return indexArray;
    }
    // 앞, 뒤(footer) 만 확인
    /**
     *
     * @param {Uint8Array} arrayBuffer
     * @return {Array.<number>}
     * @private
     */
    _getID3V2IndexArrayFast(arrayBuffer) {
        let isMatchFunctions = [
            (v) => {
                return v === 73; // I
            },
            (v) => {
                return v === 68; // D
            },
            (v) => {
                return v === 51; // 3
            },
            (v) => {
                return ((v === 2) || (v === 3) || (v === 4)); // 명세는 v < 255, 메이저 버전
            },
            (v) => {
                return v === 0; // 명세는 v < 255, 마이너 버전
            },
            (v) => {
                return (v & 0x0f) === 0; // flag 인지 % xxxx 0000
            },
            (v) => {
                return v < 128; // 안전비트 사용 중 인지 % 0xxx xxxx
            },
            (v) => {
                return v < 128; // 안전비트 사용 중 인지 % 0xxx xxxx
            },
            (v) => {
                return v < 128; // 안전비트 사용 중 인지 % 0xxx xxxx
            },
            (v) => {
                return v < 128; // 안전비트 사용 중 인지 % 0xxx xxxx
            }
        ];
        let indexArray = [];
        let boolean = true;
        for (let i = 0; i < isMatchFunctions.length && boolean; i++) {
            boolean = isMatchFunctions[i](arrayBuffer[i]) && boolean;
        }
        if (boolean) indexArray.push(0);
        else boolean = true;

        let endArrayBuffer = arrayBuffer.subarray(arrayBuffer.length - 10, arrayBuffer.length);
        for (let i = 0; i < isMatchFunctions.length && boolean; i++) {
            boolean = isMatchFunctions[i](endArrayBuffer[i]) && boolean;
        }
        if (boolean) {
            let size = this._getIntFromUint8Array(endArrayBuffer.subarray(6, endArrayBuffer.length)) + 20;
            indexArray.push(arrayBuffer.length - size);
        } else boolean = true;

        endArrayBuffer = arrayBuffer.subarray(arrayBuffer.length - 138, arrayBuffer.length - 128);
        for (let i = 0; i < isMatchFunctions.length && boolean; i++) {
            boolean = isMatchFunctions[i](endArrayBuffer[i]) && boolean;
        }
        if (boolean) {
            let size = this._getIntFromUint8Array(endArrayBuffer.subarray(6, endArrayBuffer.length)) + 20 + 128;
            indexArray.push(arrayBuffer.length - size);
        }
        return indexArray;
    }

    // ID3V1 정보 가져오기
    /**
     *
     * @param {Uint8Array} arrayBuffer
     * @return {_ID3}
     * @private
     */
    _getID3V1(arrayBuffer) {
        let ID3 = new _ID3();
        // v1.0 인지 1.1 인지
        ID3.header = this._getID3V1Header(arrayBuffer.subarray(125, 127));
        ID3.frames = this._getID3V1Frame(ID3.header, arrayBuffer);
        return ID3;
    }
    // ID3V1 헤더 정보 v1.0 인지 1.1 인지
    /**
     *
     * @param {Uint8Array} arrayBuffer
     * @return {{id3Version: string, id3Size: number}}
     * @private
     */
    _getID3V1Header(arrayBuffer) {
        /*let header = {
            id3Version: undefined,
            id3Size: 128, // ID3 데이터 전체 길이
        };*/
        let isV1_1 = (arrayBuffer[0] === 0 && arrayBuffer[1] !== 0);
        return {
            id3Version: (isV1_1) ? "ID3v1.1" : "ID3v1.0",
            id3Size: 128, // ID3 데이터 전체 길이
        };
    }
    // ID3V1 프레임 정보 가져오기
    /**
     *
     * @param {{id3Version: string, id3Size: number}} header
     * @param {Uint8Array} arrayBuffer
     * @return {Array.<{header: {frameID: string, size: number}, body: Object}>} frames
     * @private
     */
    _getID3V1Frame(header, arrayBuffer) {
        let frames = [];
        let indexes = [
            3, // title
            33, // artist
            63, // album
            93, // year
            97,  // comments
            (header.id3Version === "ID3v1.0") ? 127 : 126,  // trackNumber
            127, // genre
            128 // title
        ];
        let frameID = [
            "title",
            "artist",
            "album",
            "year",
            "comments",
            "trackNumber",
            "genre"
        ];
        let genres = {
            // ID3 기본 명세
            0: "Blues",
            1: "Classic Rock",
            2: "Country",
            3: "Dance",
            4: "Disco",
            5: "Funk",
            6: "Grunge",
            7: "Hip-Hop",
            8: "Jazz",
            9: "Metal",
            10: "New Age",
            11: "Oldies",
            12: "Other",
            13: "Pop",
            14: "R&B",
            15: "Rap",
            16: "Reggae",
            17: "Rock",
            18: "Techno",
            19: "Industrial",
            20: "Alternative",
            21: "Ska",
            22: "Death Metal",
            23: "Pranks",
            24: "Soundtrack",
            25: "Euro-Techno",
            26: "Ambient",
            27: "Trip-Hop",
            28: "Vocal",
            29: "Jazz+Funk",
            30: "Fusion",
            31: "Trance",
            32: "Classical",
            33: "Instrumental",
            34: "Acid",
            35: "House",
            36: "Game",
            37: "Sound Clip",
            38: "Gospel",
            39: "Noise",
            40: "AlternRock",
            41: "Bass",
            42: "Soul",
            43: "Punk",
            44: "Space",
            45: "Meditative",
            46: "Instrumental Pop",
            47: "Instrumental Rock",
            48: "Ethnic",
            49: "Gothic",
            50: "Darkwave",
            51: "Techno-Industrial",
            52: "Electronic",
            53: "Pop-Folk",
            54: "Eurodance",
            55: "Dream",
            56: "Southern Rock",
            57: "Comedy",
            58: "Cult",
            59: "Gangsta",
            60: "Top 40",
            61: "Christian Rap",
            62: "Pop/Funk",
            63: "Jungle",
            64: "Native American",
            65: "Cabaret",
            66: "New Wave",
            67: "Psychedelic",
            68: "Rave",
            69: "Showtunes",
            70: "Trailer",
            71: "Lo-Fi",
            72: "Tribal",
            73: "Acid Punk",
            74: "Acid Jazz",
            75: "Polka",
            76: "Retro",
            77: "Musical",
            78: "Rock & Roll",
            79: "Hard Rock",

            // winamp 자체 명세
            80: "Folk",
            81: "Folk-Rock",
            82: "National Folk",
            83: "Swing",
            84: "Fast Fusion",
            85: "Bebop",
            86: "Latin",
            87: "Revival",
            88: "Celtic",
            89: "Bluegrass",
            90: "Avantgarde",
            91: "Gothic Rock",
            92: "Progressive Rock",
            93: "Psychedelic Rock",
            94: "Symphonic Rock",
            95: "Slow Rock",
            96: "Big Band",
            97: "Chorus",
            98: "Easy Listening",
            99: "Acoustic",
            100: "Humour",
            101: "Speech",
            102: "Chanson",
            103: "Opera",
            104: "Chamber Music",
            105: "Sonata",
            106: "Symphony",
            107: "Booty Bass",
            108: "Primus",
            109: "Porn Groove",
            110: "Satire",
            111: "Slow Jam",
            112: "Club",
            113: "Tango",
            114: "Samba",
            115: "Folklore",
            116: "Ballad",
            117: "Power Ballad",
            118: "Rhythmic Soul",
            119: "Freestyle",
            120: "Duet",
            121: "Punk Rock",
            122: "Drum Solo",
            123: "A cappella",
            124: "Euro-House",
            125: "Dance Hall",
            126: "Goa",
            127: "Drum & Bass",
            128: "Club-House",
            129: "Hardcore Techno",
            130: "Terror",
            131: "Indie",
            132: "BritPop",
            133: "Negerpunk",
            134: "Polsk Punk",
            135: "Beat",
            136: "Christian Gangsta Rap",
            137: "Heavy Metal",
            138: "Black Metal",
            139: "Crossover",
            140: "Contemporary Christian",
            141: "Christian Rock",
            142: "Merengue",
            143: "Salsa",
            144: "Thrash Metal",
            145: "Anime",
            146: "Jpop",
            147: "Synthpop",
            148: "Abstract",
            149: "Art Rock",
            150: "Baroque",
            151: "Bhangra",
            152: "Big Beat",
            153: "Breakbeat",
            154: "Chillout",
            155: "Downtempo",
            156: "Dub",
            157: "EBM",
            158: "Eclectic",
            159: "Electro",
            160: "Electroclash",
            161: "Emo",
            162: "Experimental",
            163: "Garage",
            164: "Global",
            165: "IDM",
            166: "Illbient",
            167: "Industro-Goth",
            168: "Jam Band",
            169: "Krautrock",
            170: "Leftfield",
            171: "Lounge",
            172: "Math Rock",
            173: "New Romantic",
            174: "Nu-Breakz",
            175: "Post-Punk",
            176: "Post-Rock",
            177: "Psytrance",
            178: "Shoegaze",
            179: "Space Rock",
            180: "Trop Rock",
            181: "World Music",
            182: "Neoclassical",
            183: "Audiobook",
            184: "Audio Theatre",
            185: "Neue Deutsche Welle",
            186: "Podcast",
            187: "Indie-Rock",
            188: "G-Funk",
            189: "Dubstep",
            190: "Garage Rock",
            191: "Psybient",

            // 빈 값
            255: "unknown"
        };
        let decoder = new TextDecoder(this._getTextEncoding());
        for (let i = 0; i < 7; i++) {
            if (i === 5 && header.id3Version === "ID3v1.0") continue;
            let frame = {
                header: {
                    frameID: undefined,
                    size: undefined, // 프레임 길이
                },
                body: undefined
            };
            frame.header.frameID = frameID[i];
            frame.header.size = indexes[i + 1] - indexes[i];
            let bodyArray = arrayBuffer.subarray(indexes[i], indexes[i + 1]);
            let terminatedStringIndex = this._findTerminatedString(bodyArray, 0, 1, false);

            if (i === 5) frame.body = arrayBuffer[indexes[i]];
            else if (i === 6) frame.body = genres[arrayBuffer[indexes[i]]];
            else frame.body = decoder.decode(bodyArray.subarray(0, terminatedStringIndex[0].start));
            frames.push(frame);
        }
        //console.log(arrayBuffer, frames);
        return frames;
    }

    // ID3V2 정보 가져오기
    /**
     *
     * @param {Uint8Array} arrayBuffer
     * @return {_ID3}
     * @private
     */
    _getID3V2(arrayBuffer) {
        let ID3 = new _ID3();
        ID3.header = this._getID3V2Header(arrayBuffer);
        ID3.frames = this._getID3V2Frame(ID3.header, arrayBuffer);
        return ID3;
    }
    // ID3V2 헤더 정보 가져오기
    /**
     *
     * @param {Uint8Array} arrayBuffer
     * @return {{id3Version: string, flags: {unsynchronisation: boolean, extendedHeader: boolean, experimentalIndicator: boolean, footerPresent: boolean}, id3Size: number, headerSize: number, extendedHeader: {size: number}, totalHeaderSize: number}}
     * @private
     */
    _getID3V2Header(arrayBuffer) {
        let header = {
            id3Version: "",
            flags: {
                unsynchronisation: false,
                extendedHeader: false,
                experimentalIndicator: false,
                footerPresent: false
            },
            id3Size: 10, // 헤더 길이 10 (헤더와 확장 헤더 포함 ID3 데이터 전체 길이)
            headerSize: 10, // 헤더 길이 10
            extendedHeader: {
                size: 0
                // todo 확장 헤더 미완성
            },
            totalHeaderSize: 10 // 헤더 길이 10 (헤더와 확장 헤더 길이)
        };

        header.id3Version = "ID3v2." + arrayBuffer[3] + "." + arrayBuffer[4];

        // 플래그 설정
        if (arrayBuffer[5] !== 0) {
            let flagsByte = arrayBuffer[5] >> 4;
            header.flags.unsynchronisation = (flagsByte === 8);
            header.flags.extendedHeader = (flagsByte === 4);
            header.flags.experimentalIndicator = (flagsByte === 2);
            header.flags.footerPresent = (flagsByte === 1);
            //console.log("header flags:", header.flags);
        }

        // 헤더와 확장 헤더 포함 ID3 데이터 전체 길이
        header.id3Size += this._getIntFromUint8Array(arrayBuffer.subarray(6, 10), true); // 헤더엔 동기 안정 정수

        if (header.flags.extendedHeader) {
            header.extendedHeader.size = arrayBuffer[header.headerSize] << 21;
            header.extendedHeader.size += arrayBuffer[header.headerSize + 1] << 14;
            header.extendedHeader.size += arrayBuffer[header.headerSize + 2] << 7;
            header.extendedHeader.size += arrayBuffer[header.headerSize + 3] + 10;
            header.totalHeaderSize += header.extendedHeader.size;
            // todo 확장헤더 해석 미완성
            console.log("Extension headers found");
        }

        return header;
    }
    // ID3V2 프레임 정보 가져오기
    /**
     *
     * @param {{id3Version: string, flags: {unsynchronisation: boolean, extendedHeader: boolean, experimentalIndicator: boolean, footerPresent: boolean}, id3Size: number, headerSize: number, extendedHeader: {size: number}, totalHeaderSize: number}} header
     * @param {Uint8Array} arrayBuffer
     * @return {} frames
     * @private
     */
    _getID3V2Frame(header, arrayBuffer) {
        if (header.id3Version === "ID3v2.2.0") return this._getID3V2_2Frame(header, arrayBuffer);
        else return this._getID3V2_3Frame(header, arrayBuffer);
    }
    // V2.2 프레임 정보 가져오기
    /**
     *
     * @param {{id3Version: string, flags: {unsynchronisation: boolean, extendedHeader: boolean, experimentalIndicator: boolean, footerPresent: boolean}, id3Size: number, headerSize: number, extendedHeader: {size: number}, totalHeaderSize: number}} header
     * @param {Uint8Array} arrayBuffer
     * @return {Array.<{header:{frameID: string, headerSize: number, size: number}, body: object}>} frames
     * @private
     */
    _getID3V2_2Frame(header, arrayBuffer) {
        let index = header.totalHeaderSize;
        let frames = [];
        while (index < header.id3Size) {
            let frame = {
                header: {
                    frameID: undefined,
                    headerSize: 6, // 프레임 헤더 길이
                    size: 6, // 프레임 헤더 길이 포함한 프레임 길이
                },
                body: undefined
            };

            // 프레임이 끝났는지
            if (arrayBuffer[index] === 0 || (arrayBuffer[index] === 255 && arrayBuffer[index + 1] >= 224)) break;

            // 프레임 ID
            frame.header.frameID = String.fromCharCode(...arrayBuffer.subarray(index, index + 3));

            // 2.2 인 경우 헤더엔 synchsafe, 프레임헤더엔 4 byte-integer 방식.
            frame.header.size += this._getIntFromUint8Array(arrayBuffer.subarray(index + 3, index + 6), false); // 프레임 헤더 포함 프레임 전체 길이

            // frame ID로 해당 프레임 가져오기
            if((frame.body = this._getFrameBodyByFrameID(frame.header.frameID, arrayBuffer.subarray(index + frame.header.headerSize, index + frame.header.size))) === false) throw Error();

            // 다음 프레임 인덱스
            index += frame.header.size;
            frames.push(frame);
        }
        return frames;
    }
    // V2.3~2.4 프레임 정보 가져오기
    /**
     *
     * @param {{id3Version: string, flags: {unsynchronisation: boolean, extendedHeader: boolean, experimentalIndicator: boolean, footerPresent: boolean}, id3Size: number, headerSize: number, extendedHeader: {size: number}, totalHeaderSize: number}} header
     * @param {Uint8Array} arrayBuffer
     * @return {Array.<{header: {frameID: string, headerSize: number, size: number, flags: {status: {tagAlterPreservation: boolean, fileAlterPreservation: boolean, readOnly: boolean}, format: {groupingIdentity: boolean, compression: boolean, encryption: boolean, unsynchronisation: boolean, dataLengthIndicator: boolean}}}, body: object}>} frames
     * @private
     */
    _getID3V2_3Frame(header, arrayBuffer) {
        let index = header.totalHeaderSize;
        let frames = [];
        while (index < header.id3Size) {
            let frame = {
                header: {
                    frameID: undefined,
                    headerSize: 10, // 프레임 헤더 길이
                    size: 10, // 프레임 헤더 길이 포함한 프레임 길이
                    flags: {
                        status: {
                            tagAlterPreservation: false, // a
                            fileAlterPreservation: false, // b
                            readOnly: false // c
                        },
                        format: {
                            groupingIdentity: false, // h
                            compression: false, // k
                            encryption: false, // m
                            unsynchronisation: false, // n
                            dataLengthIndicator: false // p
                        }
                    }
                },
                body: undefined
            };

            // 프레임이 끝났는지
            if (arrayBuffer[index] === 0 || (arrayBuffer[index] === 255 && arrayBuffer[index + 1] >= 224)) break;

            // 프레임 ID
            frame.header.frameID = String.fromCharCode(...arrayBuffer.subarray(index, index + 4));

            // 2.4 인 경우 헤더와 프레임헤더에 모두 synchsafe 방식,
            // 2.3 인 경우 헤더엔 synchsafe, 프레임헤더엔 4 byte-integer 방식.
            frame.header.size += this._getIntFromUint8Array(arrayBuffer.subarray(index + 4, index + 8), (header.id3Version === "ID3v2.4.0")); // 프레임 헤더 포함 프레임 전체 길이
            // 플래그
            // 2.4 % 0abc 0000
            // 2.3 % abc0 0000
            if (arrayBuffer[index + 8] !== 0) {
                let flagsByte = (header.id3Version === "ID3v2.4.0") ? arrayBuffer[index + 8] >> 4 : arrayBuffer[index + 8] >> 5;
                frame.header.flags.status.tagAlterPreservation = (flagsByte === 4);
                frame.header.flags.status.fileAlterPreservation = (flagsByte === 2);
                frame.header.flags.status.readOnly = (flagsByte === 1);
                //console.log("frame header flags status:", frame.header.flags.status);
            }
            // 2.4 % 0h00 kmnp
            // 2.3 % ijk0 0000
            if (arrayBuffer[index + 9] !== 0) {
                if (header.id3Version === "ID3v2.4.0") {
                    frame.header.flags.format.groupingIdentity = ((arrayBuffer[index + 9] & 64) === 64);
                    frame.header.flags.format.compression = ((arrayBuffer[index + 9] & 8) === 8);
                    frame.header.flags.format.encryption = ((arrayBuffer[index + 9] & 4) === 4);
                    frame.header.flags.format.unsynchronisation = ((arrayBuffer[index + 9] & 2) === 2);
                    frame.header.flags.format.dataLengthIndicator = ((arrayBuffer[index + 9] & 1) === 1);
                } else {
                    frame.header.flags.format.groupingIdentity = ((arrayBuffer[index + 9] & 128) === 128);
                    frame.header.flags.format.compression = ((arrayBuffer[index + 9] & 64) === 64);
                    frame.header.flags.format.encryption = ((arrayBuffer[index + 9] & 32) === 32);
                }
                //console.log("frame header flags format:", frame.header.flags.format);
            }

            // T000 ~ TZZZ include TXXX
            if((frame.body = this._getFrameBodyByFrameID(frame.header.frameID, arrayBuffer.subarray(index + frame.header.headerSize, index + frame.header.size))) === false) throw Error();

            // 다음 프레임 인덱스
            index += frame.header.size;
            frames.push(frame);
        }
        return frames;
    }

    // frame ID로 해당 프레임 가져오기
    /**
     *
     * @param {string} frameID
     * @param {Uint8Array} frameArrayBuffer
     * @return {Object | undefined}
     * @private
     */
    _getFrameBodyByFrameID(frameID, frameArrayBuffer) {
        try {
            return this._getFrameIDFuncs1[frameID](frameArrayBuffer);
        } catch (e1) {
            try {
                return this._getFrameIDFuncs2[frameID[0]](frameArrayBuffer);
            } catch (e2) {
                console.log("Not defined: ", frameID);
            }
        }
        return undefined;
    }

    // TXXX, TXX //type3
    // todo 인코딩 문제 해결 해야하나?
    _getFrameOfUserDefinedTextInformation(arrayBuffer) {
        let frameBody = {
            textEncoding: undefined,
            description: undefined,
            information: undefined
        };
        frameBody.textEncoding = this._getTextEncoding(arrayBuffer[0]);
        let decoder = new TextDecoder(frameBody.textEncoding);

        let terminatedStringIndex =  this._findTerminatedString(arrayBuffer, 1, 1, this._isTerminatedWithZeroZero(frameBody.textEncoding));
        frameBody.description = decoder.decode(arrayBuffer.subarray(1, terminatedStringIndex[0].start));
        frameBody.information = decoder.decode(arrayBuffer.subarray(terminatedStringIndex[0].end + 1, arrayBuffer.length));

        //console.log(arrayBuffer, frameBody.textEncoding + ":" + frameBody.description + ":" + frameBody.information);
        return frameBody;
    }

    // T000 ~ TZZZ, T00 ~ TZZ exclude TXXX, TXX
    // todo 인코딩 문제 해결 해야하나?
    _getFrameOfTextInformation(arrayBuffer) {
        let frameBody = {
            textEncoding: undefined,
            information: undefined
        };
        frameBody.textEncoding = this._getTextEncoding(arrayBuffer[0]);
        let decoder = new TextDecoder(frameBody.textEncoding);

        frameBody.information = decoder.decode(arrayBuffer.subarray(1, arrayBuffer.length));
        //console.log(arrayBuffer, frameBody.textEncoding+":"+frameBody.information);
        return frameBody;
    }

    // COMM, COM //type1
    _getFrameOfComments(arrayBuffer) {
        let frameBody = {
            textEncoding: undefined,
            language: undefined,
            shortContentDescriptor: undefined,
            comments: undefined
        };

        frameBody.textEncoding = this._getTextEncoding(arrayBuffer[0]);
        let decoder = new TextDecoder(frameBody.textEncoding);
        frameBody.language = String.fromCharCode(...arrayBuffer.subarray(1, 4));

        let terminatedStringIndex =  this._findTerminatedString(arrayBuffer, 4, 1, this._isTerminatedWithZeroZero(frameBody.textEncoding));
        frameBody.shortContentDescriptor = decoder.decode(arrayBuffer.subarray(4, terminatedStringIndex[0].start));
        frameBody.comments = decoder.decode(arrayBuffer.subarray(terminatedStringIndex[0].end + 1, arrayBuffer.length));

        //console.log(frameBody.language + ":" + frameBody.shortContentDescriptor + ":" + frameBody.comments);
        return frameBody;
    }

    // APIC, PIC
    _getFrameOfAttachedPicture(arrayBuffer, isV2_2) {
        /*let frameBody = {
            textEncoding: undefined,
            mimeType: undefined,
            pictureType: undefined,
            description: undefined,
            pictureData: undefined
        };*/
        let frameBody = new _Picture();
        /*let pictureType = [
            "Other",
            "32x32 pixels 'file icon' (PNG only)",
            "Other file icon",
            "Cover (front)",
            "Cover (back)",
            "Leaflet page",
            "Media (e.g. label side of CD)",
            "Lead artist/lead performer/soloist",
            "Artist/performer",
            "Conductor",
            "Band/Orchestra",
            "Composer",
            "Lyricist/text writer",
            "Recording Location",
            "During recording",
            "During performance",
            "Movie/video screen capture",
            "A bright coloured fish",
            "Illustration",
            "Band/artist logotype",
            "Publisher/Studio logotype"
        ];*/
        frameBody.textEncoding = this._getTextEncoding(arrayBuffer[0]);
        let decoder = new TextDecoder(frameBody.textEncoding);

        if (isV2_2) {
            let terminatedStringIndex =  this._findTerminatedString(arrayBuffer, 5, 1, this._isTerminatedWithZeroZero(frameBody.textEncoding));
            frameBody.mimeType = String.fromCharCode(...arrayBuffer.subarray(1, 4));
            //frameBody.pictureType = pictureType[arrayBuffer[4]];
            frameBody.pictureType = arrayBuffer[4];
            frameBody.description = decoder.decode(arrayBuffer.subarray(5, terminatedStringIndex[0].start));
            frameBody.pictureData = arrayBuffer.subarray(terminatedStringIndex[0].end + 1, arrayBuffer.length);
        } else {
            let terminatedStringIndex1 =  this._findTerminatedString(arrayBuffer, 1, 1, false);
            let terminatedStringIndex2 =  this._findTerminatedString(arrayBuffer, terminatedStringIndex1[0].end + 2, 1, this._isTerminatedWithZeroZero(frameBody.textEncoding));
            frameBody.mimeType = String.fromCharCode(...arrayBuffer.subarray(1, terminatedStringIndex1[0].start));
            //frameBody.pictureType = pictureType[arrayBuffer[terminatedStringIndex1[0].end + 1]];
            frameBody.pictureType = arrayBuffer[terminatedStringIndex1[0].end + 1];
            frameBody.description = decoder.decode(arrayBuffer.subarray(terminatedStringIndex1[0].end + 2, terminatedStringIndex2[0].start));
            frameBody.pictureData = arrayBuffer.subarray(terminatedStringIndex2[0].end + 1, arrayBuffer.length);
        }
        //console.log(arrayBuffer.subarray(0, 40));
        //console.log("/"+frameBody.textEncoding+":"+frameBody.mimeType+":"+frameBody.pictureType+":"+frameBody.description+"/", frameBody);
        return frameBody;
    }

    // USLT, ULT //type1
    _getFrameOfUnsynchronisedLyricsTranscription(arrayBuffer) {
        let frameBody = {
            textEncoding: undefined,
            language: undefined,
            contentDescriptor: undefined,
            lyrics: undefined
        };
        frameBody.textEncoding = this._getTextEncoding(arrayBuffer[0]);
        let decoder = new TextDecoder(frameBody.textEncoding);

        frameBody.language = String.fromCharCode(...arrayBuffer.subarray(1, 4));

        let terminatedStringIndex =  this._findTerminatedString(arrayBuffer, 4, 1, this._isTerminatedWithZeroZero(frameBody.textEncoding));
        frameBody.contentDescriptor = decoder.decode(arrayBuffer.subarray(4, terminatedStringIndex[0].start));
        frameBody.lyrics = decoder.decode(arrayBuffer.subarray(terminatedStringIndex[0].end + 1, arrayBuffer.length));
        //console.log(frameBody.textEncoding + ":" + frameBody.language + ":" + frameBody.contentDescriptor + ":" + frameBody.lyrics);
        return frameBody;
    }

    // PRIV //type2
    _getFrameOfPrivate(arrayBuffer) {
        let frameBody = {
            ownerIdentifier: undefined,
            privateData: undefined
        };
        let i = arrayBuffer.indexOf(0);
        frameBody.ownerIdentifier = String.fromCharCode(...arrayBuffer.subarray(0, i));
        frameBody.privateData = arrayBuffer.subarray(i + 1, arrayBuffer.length);
        //console.log(arrayBuffer, frameBody.ownerIdentifier, frameBody.privateData);
        return frameBody;
    }

    // UFID, UFI //type2
    _getFrameOfUniqueFileIdentifier(arrayBuffer) {
        let frameBody = {
            ownerIdentifier: undefined,
            identifier: undefined
        };
        let i = arrayBuffer.indexOf(0);
        frameBody.ownerIdentifier = String.fromCharCode(...arrayBuffer.subarray(0, i));
        frameBody.identifier = arrayBuffer.subarray(i + 1, arrayBuffer.length);
        //console.log(arrayBuffer, frameBody.ownerIdentifier,frameBody.identifier);
        return frameBody;
    }

    // WXXX, WXX // type3-1
    _getFrameOfUserDefinedURLLink(arrayBuffer) {
        let frameBody = {
            textEncoding: undefined,
            description: undefined,
            url: undefined
        };
        frameBody.textEncoding = this._getTextEncoding(arrayBuffer[0]);
        let decoder = new TextDecoder(frameBody.textEncoding);

        let terminatedStringIndex =  this._findTerminatedString(arrayBuffer, 1, 1, this._isTerminatedWithZeroZero(frameBody.textEncoding));
        frameBody.description = decoder.decode(arrayBuffer.subarray(1, terminatedStringIndex[0].start));
        frameBody.url = String.fromCharCode(...arrayBuffer.subarray(terminatedStringIndex[0].end + 1, arrayBuffer.length));
        //console.log(arrayBuffer, frameBody.textEncoding + ":" + frameBody.description + ":" + frameBody.url);
        return frameBody;
    }

    // W000 ~ WZZZ, W00 ~ WZZ exclude WXXX, WXX
    _getFrameOfURLLink(arrayBuffer) {
        let frameBody = {
            url: undefined
        };
        let i = arrayBuffer.indexOf(0);
        frameBody.url = String.fromCharCode(...arrayBuffer.subarray(0, ((i === -1) ? arrayBuffer.length : i)));
        //console.log(arrayBuffer, frameBody.url);
        return frameBody;
    }

    // SYLT, SLT
    _getFrameOfSynchronisedLyrics(arrayBuffer) {
        let frameBody = {
            textEncoding: undefined,
            language: undefined,
            timeStampFormat: undefined,
            contentType: undefined,
            contentDescriptor: undefined,
            synchronisedLyrics: {
                lyrics: undefined,
                time: undefined
            }
        };
        let timeStampFormat = [
            "using MPEG [MPEG] frames as unit",
            "using milliseconds as unit"
        ];
        let contentType = [
            "other",
            "lyrics",
            "text transcription",
            "movement/part name (e.g. \"Adagio\")",
            "events (e.g. \"Don Quijote enters the stage\")",
            "chord (e.g. \"Bb F Fsus\")",
            "trivia/'pop up' information",
            "URLs to webpages",
            "URLs to images",
        ];

        frameBody.textEncoding = this._getTextEncoding(arrayBuffer[0]);
        let decoder = new TextDecoder(frameBody.textEncoding);

        frameBody.language = String.fromCharCode(...arrayBuffer.subarray(1, 4));

        frameBody.timeStampFormat = timeStampFormat[arrayBuffer[4] - 1];
        frameBody.contentType = contentType[arrayBuffer[5]];
        let terminatedStringIndex1 = this._findTerminatedString(arrayBuffer, 6, 1, this._isTerminatedWithZeroZero(frameBody.textEncoding));
        frameBody.contentDescriptor = decoder.decode(arrayBuffer.subarray(6, terminatedStringIndex1[0].start));

        let terminatedStringIndex2 = this._findTerminatedString(arrayBuffer, terminatedStringIndex1[0].end + 1, arrayBuffer.length, this._isTerminatedWithZeroZero(frameBody.textEncoding), 4);
        frameBody.synchronisedLyrics = [];
        frameBody.synchronisedLyrics.push({
            lyrics: decoder.decode(arrayBuffer.subarray(terminatedStringIndex1[0].end + 1, terminatedStringIndex2[0].start)),
            time: this._getIntFromUint8Array([0, 0, 0, 0], false)
        });
        for (let i = 0; i < terminatedStringIndex2.length - 1; i++) {
            frameBody.synchronisedLyrics.push({
                lyrics: decoder.decode(arrayBuffer.subarray(terminatedStringIndex2[i].end + 5, terminatedStringIndex2[i + 1].start)),
                time: this._getIntFromUint8Array(arrayBuffer.subarray(terminatedStringIndex2[i].end + 1, terminatedStringIndex2[i].end + 5), false)
            });
        }
        frameBody.synchronisedLyrics.push({
            lyrics: "",
            time: this._getIntFromUint8Array(arrayBuffer.subarray(terminatedStringIndex2[terminatedStringIndex2.length - 1].end + 1, terminatedStringIndex2[terminatedStringIndex2.length - 1].end + 5), false)
        });
        //console.log(arrayBuffer, frameBody);
        return frameBody;
    }

    // MCDI
    _getFrameOfMusicCDIdentifier(arrayBuffer) {
        let frameBody = {
            CDTOC: undefined
        };
        frameBody.CDTOC = arrayBuffer;
        //console.log(frameBody.CDTOC);
        return frameBody;
    }

    // POPM, POP
    _getFrameOfPopularimeter(arrayBuffer) {
        let frameBody = {
            emailToUser: undefined,
            rating: undefined,
            counter: 0
        };
        let i = arrayBuffer.indexOf(0);
        frameBody.emailToUser = String.fromCharCode(...arrayBuffer.subarray(0, i));
        frameBody.rating = arrayBuffer[i + 1];
        if (arrayBuffer.length - i + 2 >= 4) {
            arrayBuffer.subarray(i + 2, arrayBuffer.length).forEach((v, i, o) => {
                frameBody.counter += v * Math.pow(2, (o.length - i - 1) * 8);
            });
        }
        //console.log(arrayBuffer, frameBody);
        return frameBody;
    }

    // PCNT, CNT
    _getFrameOfPlayCounter(arrayBuffer) {
        let frameBody = {
            counter: 0
        };
        if (arrayBuffer.length >= 4) {
            arrayBuffer.forEach((v, i, o) => {
                frameBody.counter += v * Math.pow(2, (o.length - i - 1) * 8);
            });
        }
        //console.log(arrayBuffer, frameBody);
        return frameBody;
    }

    // GEOB, GEO
    _getFrameOfGeneralEncapsulatedObject(arrayBuffer) {
        let frameBody = {
            textEncoding: undefined,
            mimeType: undefined,
            filename: undefined,
            contentDescription: undefined,
            encapsulatedObject: undefined
        };
        frameBody.textEncoding = this._getTextEncoding(arrayBuffer[0]);

        let decoder = new TextDecoder(frameBody.textEncoding);
        let terminatedStringIndex1 =  this._findTerminatedString(arrayBuffer, 1, 1, false);
        let terminatedStringIndex2 =  this._findTerminatedString(arrayBuffer, terminatedStringIndex1[0].end + 1, 2, this._isTerminatedWithZeroZero(frameBody.textEncoding));
        frameBody.mimeType = String.fromCharCode(...arrayBuffer.subarray(1, terminatedStringIndex1[0].start));
        frameBody.filename = decoder.decode(arrayBuffer.subarray(terminatedStringIndex1[0].end + 1, terminatedStringIndex2[0].start));
        frameBody.contentDescription = decoder.decode(arrayBuffer.subarray(terminatedStringIndex2[0].end + 1, terminatedStringIndex2[1].start));
        frameBody.encapsulatedObject = arrayBuffer.subarray(terminatedStringIndex2[1].end + 1, arrayBuffer.length);
        //console.log(frameBody);
        return frameBody;
    }

    // RVAD // todo 명세 이상함
    _getFrameOfRelativeVolumeAdjustment(arrayBuffer) {
        let frameBody = {
        };
        console.log("RVAD", arrayBuffer, frameBody);
        return frameBody;
    }

    // Text Encoding 명세에 따라
    /**
     *
     * @param byte
     * @return {string}
     * @private
     */
    _getTextEncoding(byte) {
        // 기본 인코딩 설정으로 잘못 넣은 데이터일 경우만 변경 가능
        if (this.getDefaultCharset !== undefined && (byte === 0 || byte === undefined)) return this.getDefaultCharset;
        switch (byte) {
            case 1: return "UTF-16";
            case 2: return "UTF-16BE";
            case 3: return "UTF-8";
            default: return "ISO-8859-1";
        }
    }

    // 문자열 종료 문자가 $00 00인지
    /**
     *
     * @param {string} textEncoding
     * @return {boolean}
     * @private
     */
    _isTerminatedWithZeroZero(textEncoding) {
        switch (textEncoding) {
            case "UTF-16":
            case "UTF-16BE": return true;
            case "UTF-8":
            default: return false;
        }
    }

    // 종료문자열 찾기 시작 부분 인덱스 리턴
    /**
     *
     * @param {Uint8Array} uInt8Array
     * @param {number} fromIndex
     * @param {number} count
     * @param {Boolean} [isZeroZero]
     * @param {number} [step]
     * @return {Array.<{start: index, end: index}>}
     * @private
     */
    _findTerminatedString(uInt8Array, fromIndex, count, isZeroZero, step) {
        let returnArr = [];
        let index = fromIndex;
        let exp = fromIndex % 2; // 시작 바이트 기준으로 utf16 짝수 계산
        let _step = ((step === undefined) ? 0 : step) + 1; // end 인덱스 다음 step 바이트 무시
        if (isZeroZero) {
            for (let i = 0; i < count; i++) {
                index = uInt8Array.indexOf(0, index);
                if (index === -1) break;
                index++;
                if ((index % 2) + exp === 1 && uInt8Array[index] === 0) { // utf16의 뒷바이트이면, 그 바이트가 0이면
                    returnArr.push({
                        start: index - 1,
                        end: index
                    });
                    index+=_step;
                }
                else i--;
            }
        } else {
            for (let i = 0; i < count; i++) {
                index = uInt8Array.indexOf(0, index);
                if (index === -1) break;
                returnArr.push({
                    start: index,
                    end: index
                });
                index+=_step;
            }
        }
        if (returnArr.length === 0) returnArr.push({ start: uInt8Array.length, end: uInt8Array.length });
        return returnArr;
    }

    // Uint8Array 바이트로 정수 구하기, 동기 안정 정수인지
    /**
     *
     * @param {Uint32Array} uInt8Array
     * @param {Boolean} isSynchsafe
     * @return {number}
     * @private
     */
    _getIntFromUint8Array(uInt8Array, isSynchsafe) {
        let number = 0;
        let cal = (isSynchsafe) ? (j) => { return ((uInt8Array.length - 1) * 7) - (7 * j); } : (j) => { return ((uInt8Array.length - 1) * 8) - (8 * j); };
        for (let i = 0; i < uInt8Array.length; i++) {
            number += uInt8Array[i] << cal(i);
        }
        return number;
    }
}

export default OExtractID3;