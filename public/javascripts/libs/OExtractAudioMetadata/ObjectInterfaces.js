/**
 * @interface
 */
class PictureObject {
    get mimeType() {
        return this._mimeType;
    }
    set mimeType(string) {
        if (string.constructor.name !== "String") throw TypeError("Not a String");
        this._mimeType = string;
    }
    get pictureData() {
        return this._pictureData;
    }
    set pictureData(Array) {
        if (Array.constructor.__proto__.name !== "ArrayBuffer" && Array.constructor.__proto__.name !== "TypedArray") throw TypeError("Not an ArrayBuffer or TypedArray");
        this._pictureData = Array;
    }
}

/**
 * @interface
 */
class TagObject {
    get tagName() {
        return this.__tagName;
    }
    set tagName(string) {
        if (string.constructor.name !== "String") throw TypeError("Not a String");
        this.__tagName = string;
    }
    get toObject() {

    }
}
export { TagObject, PictureObject };