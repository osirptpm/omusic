class _TracksDB {
    constructor() {
        this.DB = {};
    }

    set DB(db) {
        this._db = db;
    }
    get DB() {
        return this._db;
    }

    // 해당 배열 리턴
    getArray(arrayName) {
        if (arrayName.constructor.name !== "String") throw TypeError("Not a String");
        return this.DB[arrayName];
    }
    // 객체 ID 속성으로 객체 찾기
    getObjectFromArrayByID(arrayName, objectID) {
        if (arrayName.constructor.name !== "String") throw TypeError("Not a String");
        if (objectID.constructor.name !== "Number") throw TypeError("Not a Number");
        return this.DB[arrayName].find(o => {
            return o.id === objectID;
        });
    }

    // 배열 추가
    addArray(arrayName) {
        if (arrayName.constructor.name !== "String") throw TypeError("Not a String");
        this.DB[arrayName] = [];
    }
    // 배열 삭제
    removeArray(arrayName) {
        if (arrayName.constructor.name !== "String") throw TypeError("Not a String");
        delete this.DB[arrayName];
    }

    // 배열에 객체 추가
    addObjectToArray(arrayName, object) {
        if (typeof object !== "object" && object.id !== undefined) throw TypeError("It is not a validated object. \"id\" attribute is required.");
        this.DB[arrayName].push(object);
    }
    // 배열에서 객체 제거
    removeObjectFromArray(arrayName, objectID) {
        if (objectID.constructor.name !== "Number") throw TypeError("Not a Number");
        this.DB[arrayName] = this.DB[arrayName].filter(o => {
            return o.id !== objectID;
        });
    }
}

class _SongObject {
    constructor() {
        this.tracks = [];
        this.isBlobURL = false;
        if (arguments.length > 0) {
            if (arguments[0].constructor.name === "File") this.file = arguments[0];
            else if (arguments[0].constructor.name === "String") this.src = arguments[0];
        }
    }
    set id(id) {
        if (id.constructor.name !== "Number") throw TypeError("Not a Number");
        this._id = id;
    }
    get id() { return this._id; }
    set src(src) {
        if (src.constructor.name !== "String") throw TypeError("Not a String");
        this._src = src;
    }
    get src() { return this._src; }
    set file(file) {
        if (file.constructor.name !== "File") throw TypeError("Not a File");
        this._file = file;
        this.src = URL.createObjectURL(file);
        this.isBlobURL = true;
    }
    get file() { return this._file; }
    set tag(tag) { this._tag = tag; }
    get tag() { return this._tag; }
    set tracks(tracks) {
        if (tracks.constructor.name !== "Array") throw TypeError("Not a Array");
        this._tracks = tracks;
    }
    get tracks() { return this._tracks; }
    set isBlobURL(boolean) {
        if (boolean.constructor.name !== "Boolean") throw TypeError("Not a Boolean");
        this._isBlobURL = boolean;
    }
    get isBlobURL() { return this._isBlobURL; }

    addTrack(trackID) {
        if (trackID.constructor.name !== "Number") throw TypeError("Not a Number");
        this.tracks.push(trackID);
    }
    removeTrack(trackID) {
        if (trackID.constructor.name !== "Number") throw TypeError("Not a Number");
        this.tracks = this.tracks.filter(trackId => {
            return trackId !== trackID;
        });
    }
}

class _TrackObject {
    constructor(name) {
        this.name = name;
        this.songs = [];
        this.lastModified = new Date().getTime();
    }

    set id(id) {
        if (id.constructor.name !== "Number") throw TypeError("Not a Number");
        this._id = id;
        this.lastModified = new Date().getTime();
    }
    get id() { return this._id; }
    set name(name) {
        if (name.constructor.name !== "String") throw TypeError("Not a String");
        this._name = name;
        this.lastModified = new Date().getTime();
    }
    get name() { return this._name; }
    set songs(songs) {
        if (songs.constructor.name !== "Array") throw TypeError("Not a Array");
        this._songs = songs;
        this.lastModified = new Date().getTime();
    }
    get songs() { return this._songs; }
    set lastModified(lastModified) {
        if (lastModified.constructor.name !== "Number") throw TypeError("Not a Number");
        this._lastModified = lastModified;
    }
    get lastModified() { return this._lastModified; }

    addSong(songID) {
        if (songID.constructor.name !== "Number") throw TypeError("Not a Number");
        this.songs.push(songID);
        this.lastModified = new Date().getTime();
    }
    removeSong(songID) {
        if (songID.constructor.name !== "Number") throw TypeError("Not a Number");
        this.songs = this.songs.filter(songId => {
            return songId !== songID;
        });
        this.lastModified = new Date().getTime();
    }
}

class OTrackManager {
    constructor() {
        this.__songIDIndex = 0;
        this.__trackIDIndex = 0;
        this.__db = new _TracksDB();
        this.__SONGS = "songs";
        this.__TRACKS = "tracks";
        this._DB.addArray(this._SONGS);
        this._DB.addArray(this._TRACKS);
        this.onChangeTrack = () => {};
    }

    // 비공개 속성
    /**
     *
     * @return {_TracksDB}
     * @private
     */
    get _DB() {
        return this.__db;
    }
    /**
     *
     * @return {string}
     * @private
     */
    get _SONGS() {
        return this.__SONGS;
    }
    /**
     *
     * @return {string}
     * @private
     */
    get _TRACKS() {
        return this.__TRACKS;
    }

    /**
     *
     * @return {number}
     * @private
     */
    get _getNewSongID() {
        return this.__songIDIndex++;
    }
    /**
     *
     * @return {number}
     * @private
     */
    get _getNewTrackID() {
        return this.__trackIDIndex++;
    }

    // 현재 트랙 곡 변경 콜백
    /**
     *
     * @return {Function}
     * @private
     */
    get _changeTrackCallback() {
        return this.__changeTrackCallback;
    }

    // 공개 속성
    /**
     * @returns {_TrackObject} trackObject
     */
    get getCurrentTrack() {
        return this._getTrackByID(this.__currentTrackID);
    }
    /**
     *
     * @param {number} trackID
     */
    set setCurrentTrack(trackID) {
        if (trackID.constructor.name !== "Number") throw TypeError("Not a Number");
        this.__currentTrackID = trackID;
        this._changeTrackCallback(trackID);
    }

    // 현재 트랙 곡 변경 콜백
    /**
     *
     * @param {function} callback
     */
    set onChangeTrack(callback) {
        if (callback.constructor.name !== "Function") throw TypeError("Not a Function");
        this.__changeTrackCallback = callback;
    }

    // 사용자 호출 메소드

    // 트랙 관련 메소드 //

    // 곡 정보 가져오기
    /**
     *
     * @returns {Array.<_SongObject>} songObjects
     */
    getSongs() {
        return this._DB.getArray(this._SONGS);
    }

    /**
     *
     * @param {number} trackID
     * @returns {Array.<_SongObject>} songObjects
     */
    getSongsFromTrackID(trackID) {
        let songs = [];
        let trackObject = this._getTrackByID(trackID);
        trackObject.songs.forEach(songId => {
            let songObject = this._getSongByID(songId);
            songs.push(songObject);
        });
        return songs;
    }
    /**
     *
     * @param {number} songID
     * @returns {_SongObject} songObject
     */
    getSongByID(songID) {
        return this._getSongByID(songID);
    }
    /**
     *
     * @param {Array.<number>} songIDArray
     * @returns {Array.<_SongObject>} songObjects
     */
    getSongsByIDArray(songIDArray) {
        let songs = [];
        songIDArray.forEach(songId => {
            let songObject = this.getSongByID(songId);
            songs.push(songObject);
        });
        return songs;
    }

    // 트랙 정보 가져오기
    /**
     *
     * @returns {Array.<_TrackObject>} trackObjects
     */
    getTracks() {
        return this._DB.getArray(this._TRACKS);
    }
    /**
     *
     * @param {number} trackID
     * @returns {_TrackObject} trackObject
     */
    getTrackByID(trackID) {
        return this._getTrackByID(trackID);
    }
    /**
     *
     * @param {string} trackName
     * @returns {_TrackObject} trackObject
     */
    getTrackByName(trackName) {
        let trackObject = this._getTrackByName(trackName);
        if (trackObject === undefined) throw Error("Track dose not exist");
        return trackObject;
    }

    // 트랙 추가
    /**
     *
     * @param {string} trackName
     * @returns {number} trackID
     */
    addTrack(trackName) {
        if (this._getTrackByName(trackName) !== undefined) return false;
        let newID = this._getNewTrackID;
        let trackObject = new _TrackObject(trackName);
        trackObject.id = newID;
        this._DB.addObjectToArray(this._TRACKS, trackObject);
        return newID;
    }

    // 트랙 제거
    /**
     *
     * @param {number} trackID
     */
    removeTrack(trackID) {
        this._DB.removeObjectFromArray(this._TRACKS, trackID);
    }

    // 곡 추가
    /**
     *
     * @param {string} src
     * @returns {number} songID
     */
    addSongAsSrc(src) {
        return this._addSongAsSrc(src);
    }
    /**
     *
     * @param {File} file
     * @returns {number} songID
     */
    addSongAsFile(file) {
        return this._addSongAsFile(file);
    }
    /**
     *
     * @param {number} trackID
     * @param {string} src
     * @returns {number} songID
     */
    addSongToTrackAsSrc(trackID, src) {
        let songID = this._addSongAsSrc(src);
        this._addSongToTrack(trackID, songID);
        return songID;
    }
    /**
     *
     * @param {number} trackID
     * @param {File} file
     * @returns {number} songID
     */
    addSongToTrackAsFile(trackID, file) {
        let songID = this._addSongAsFile(file);
        this._addSongToTrack(trackID, songID);
        return songID;
    }
    /**
     *
     * @param {number} trackID
     * @param {number} songID
     */
    addSongToTrackAsID(trackID, songID) {
        this._addSongToTrack(trackID, songID);
    }

    // 복수 곡 추가
    /**
     *
     * @param {Array.<string>} srcArray
     * @returns {Array.<number>} songIDArray
     */
    addSongAsSrcs(srcArray) {
        let songIDArray = [];
        srcArray.forEach(src => {
            let songID = this.addSongAsSrc(src);
            songIDArray.push(songID);
        });
        return songIDArray;
    }
    /**
     *
     * @param {Array.<File> | FileList} fileArray
     * @returns {Array.<number>} songIDArray
     */
    addSongAsFiles(fileArray) {
        let songIDArray = [];
        for (let i = 0; i < fileArray.length; i++) {
            let songID = this.addSongAsFile(fileArray[i]);
            songIDArray.push(songID);
        }
        return songIDArray;
    }
    /**
     *
     * @param {number} trackID
     * @param {Array.<string>} srcArray
     * @returns {Array.<number>} songIDArray
     */
    addSongToTrackAsSrcs(trackID, srcArray) {
        let songIDArray = [];
        srcArray.forEach(src => {
            let songID = this.addSongToTrackAsSrc(trackID, src);
            songIDArray.push(songID);
        });
        return songIDArray;
    }
    /**
     *
     * @param {number} trackID
     * @param {Array.<File> | FileList} fileArray
     * @returns {Array.<number>} songIDArray
     */
    addSongToTrackAsFiles(trackID, fileArray) {
        let songIDArray = [];
        for (let i = 0; i < fileArray.length; i++) {
            let songID = this.addSongToTrackAsFile(trackID, fileArray[i]);
            songIDArray.push(songID);
        }
        return songIDArray;
    }

    // 곡 제거
    /**
     *
     * @param {number} songID
     */
    removeSong(songID) {
        this._removeSong(songID);
    }
    /**
     *
     * @param {number} trackID
     * @param {number} songID
     */
    removeSongFromTrack(trackID, songID) {
        this._removeSongFromTrack(trackID, songID);
    }

    // 복수 곡 제거
    /**
     *
     * @param {Array.<number>} songIDArray
     */
    removeSongs(songIDArray) {
        songIDArray.forEach(songId => {
            this.removeSong(songId);
        })
    }
    /**
     *
     * @param {number} trackID
     * @param {Array.<number>} songIDArray
     */
    removeSongsFromTrack(trackID, songIDArray) {
        songIDArray.forEach(songId => {
            this.removeSongFromTrack(trackID, songId);
        })
    }

    // 비공개 메소드

    // 트랙 관련 메소드 //

    // 트랙 찾기
    /**
     *
     * @param {number} trackID
     * @return {_TrackObject} trackObject
     * @private
     */
    _getTrackByID(trackID) {
        let trackObject = this._DB.getObjectFromArrayByID(this._TRACKS, trackID);
        if (trackObject === undefined) throw Error("Track dose not exist");
        return trackObject;
    }
    /**
     *
     * @param {string} trackName
     * @return {undefined | _TrackObject}
     * @private
     */
    _getTrackByName(trackName) {
        let  tracks = this._DB.getArray(this._TRACKS);
        return tracks.find(trackObject => {
            return trackObject.name === trackName;
        });
    }

    // 해당 곡 찾기
    /**
     *
     * @param {number} songID
     * @return {_SongObject} songObject
     * @private
     */
    _getSongByID(songID) {
        let songObject = this._DB.getObjectFromArrayByID(this._SONGS, songID);
        if (songObject === undefined) throw Error("The song dose not exist");
        return songObject;
    }

    // 곡 추가, 곡 인덱스 리턴
    /**
     *
     * @param {_SongObject} songObject
     * @return {number} songID
     * @private
     */
    _addSong(songObject) {
        let newID = this._getNewSongID;
        songObject.id = newID;
        this._DB.addObjectToArray(this._SONGS, songObject);
        return newID;
    }
    // 곡 src, file 로 추가, 곡 인덱스 리턴
    /**
     *
     * @param {string} src
     * @return {number} songID
     * @private
     */
    _addSongAsSrc(src) {
        let songObject = new _SongObject(src);
        return this._addSong(songObject);
    }
    /**
     *
     * @param {File} file
     * @return {number} songID
     * @private
     */
    _addSongAsFile(file) {
        let songObject = new _SongObject(file);
        return this._addSong(songObject);
    }
    // 곡을 해당 트랙에 추가
    /**
     *
     * @param {number} trackID
     * @param {number} songID
     * @private
     */
    _addSongToTrack(trackID, songID) {
        let trackObject = this._getTrackByID(trackID);
        trackObject.addSong(songID);
        let songObject = this._getSongByID(songID);
        songObject.addTrack(trackID);
        this._changeTrackCallback(trackID);
    }

    // 곡 제거
    /**
     *
     * @param {number} songID
     * @return {boolean}
     * @private
     */
    _removeSong(songID) {
        let songObject = this._getSongByID(songID);
        if (songObject === undefined) return false;
        let tracks = songObject.tracks;
        tracks.forEach(trackID => {
            let trackObject = this._getTrackByID(trackID);
            trackObject.removeSong(songID);
        });
        if (songObject.isBlobURL) URL.revokeObjectURL(songObject.src);
        this._DB.removeObjectFromArray(this._SONGS, songObject.id);
        return true;
    }
    // 곡을 해당 트랙에서 제거
    /**
     *
     * @param {number} trackID
     * @param {number} songID
     * @private
     */
    _removeSongFromTrack(trackID, songID) {
        let songObject = this._getSongByID(songID);
        songObject.removeTrack(trackID);
        let trackObject = this._getTrackByID(trackID);
        trackObject.removeSong(songID);
        this._changeTrackCallback(trackID);
    }
}


export default OTrackManager;