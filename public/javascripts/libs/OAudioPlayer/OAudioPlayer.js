class _CircleArray extends Array {
    constructor() {
        super();
        this.pointer = 0;
    }
    /**
     *
     * @return {number}
     */
    get pointer() {
        return this._pointer;
    }
    /**
     *
     * @param {number} index
     */
    set pointer(index) {
        if (this.length === 0 || index >= this.length) index = 0;
        else if (index < 0) index = this.length - 1;
        this._pointer = index;
    }
    /**
     *
     * @return {object}
     */
    get peek() {
        return this[this.pointer];
    }
    /**
     *
     * @return {object}
     */
    get nextPeek() {
        this.pointer++;
        return this[this.pointer];
    }
    /**
     *
     * @return {object}
     */
    get previousPeek() {
        this.pointer--;
        return this[this.pointer];
    }
    /**
     *
     * @return {object}
     */
    get randomPeek() {
        this.pointer = Math.floor(Math.random() * this.length);
        return this.peek;
    }
}

class _Tracks extends _CircleArray {
    /**
     * @typedef {Object} song_object
     * @property {string} src
     */
    /**
     *
     * @param {Array.<song_object>} songObjectArray
     * @constructor
     */
    constructor(songObjectArray) {
        super();
        if (songObjectArray.constructor.name === "Array"
            && (songObjectArray.length === 0 || (songObjectArray.length > 0 && songObjectArray[0].src !== undefined))) {
            this.push(...songObjectArray);
        } else throw TypeError("It is not a validated object. ID attribute is required.");
    }
}

class _LoopMode {
    static get ALL() { return "All" }
    static get ONE() { return "One" }
    static get NOT() { return "Not" }
}

class OAudioPlayer extends Audio {
    constructor() {
        super();
        this._playing = true;
        this.__loopMode = new _CircleArray();
        this.__loopMode.push(_LoopMode.ALL, _LoopMode.ONE, _LoopMode.NOT);
        // LoopMode 에 따른 연속 재생
        this.addEventListener("ended", () => { this.next() });
    }

    // 비공개 속성
    /**
     *
     * @param {Boolean} boolean
     * @private
     */
    set _playing(boolean) {
        if (boolean.constructor.name !== "Boolean") throw TypeError("Not a Boolean");
        this.__playing = boolean;
    }
    /**
     *
     * @return {Boolean}
     * @private
     */
    get _playing() {
        return this.__playing;
    }

    /**
     *
     * @return {_CircleArray}
     * @private
     */
    get _loopMode() {
        return this.__loopMode;
    }

    /**
     *
     * @param {Boolean} boolean
     * @private
     */
    set _shuffle(boolean) {
        if (boolean.constructor.name !== "Boolean") throw TypeError("Not a Boolean");
        this.__shuffle = boolean;
    }
    /**
     *
     * @return {Boolean}
     * @private
     */
    get _shuffle() {
        return this.__shuffle;
    }

    // 공개 속성
    /**
     *
     * @param {Array} songObjectArray
     */
    set setTrack(songObjectArray) {
        let track = new _Tracks(songObjectArray);
        if (this.getTrack === undefined && track.length > 0) {
            this.src = track.peek.src;
        }
        this.__currentTrack = track;
    }
    /**
     *
     * @return {_Tracks}
     */
    get getTrack() {
        return this.__currentTrack;
    }

    /**
     *
     * @return {string}
     */
    get getLoopMode() {
        return this._loopMode.peek;
    }

    /**
     *
     * @return {song_object}
     */
    get getCurrentSong() {
        return this.getTrack.peek;
    }


    // 사용자 호출 메소드

    // 재생
    /**
     *
     * @return {Promise<void>}
     */
    play() {
        this._playing = true;
        return super.play();
    }
    // 일시 중지
    pause() {
        this._playing = false;
        return super.pause();
    }
    // 재생 토글
    PP() {
        if (super.paused) return this.play();
        else return this.pause();
    }
    // 다음 곡
    next() {
        switch (this.getLoopMode) {
            case _LoopMode.ALL: {
                this.src = (this._shuffle) ? this.getTrack.randomPeek.src : this.getTrack.nextPeek.src;
                break;
            }
            case _LoopMode.ONE: this.src = this.getTrack.peek.src; break;
            case _LoopMode.NOT: {
                if (this.getTrack.length - 1 > this.getTrack.pointer)
                    this.src = (this._shuffle) ? this.getTrack.randomPeek.src : this.getTrack.nextPeek.src;
                else this._playing = false;
                break;
            }
        }
        if (this._playing) return this.play();
    }
    // 이전 곡
    previous() {
        switch (this.getLoopMode) {
            case _LoopMode.ALL: {
                this.src = (this._shuffle) ? this.getTrack.randomPeek.src : this.getTrack.previousPeek.src;
                break;
            }
            case _LoopMode.ONE: this.src = this.getTrack.peek.src; break;
            case _LoopMode.NOT: {
                if (0 < this.getTrack.pointer)
                    this.src = (this._shuffle) ? this.getTrack.randomPeek.src : this.getTrack.previousPeek.src;
                else this._playing = false;
                break;
            }
        }
        if (this._playing) return this.play();
    }

    //  전체 반복, 한곡 반복, 무반복
    /**
     *
     * @return {Object}
     */
    changeLoopMode() {
        return this._loopMode.nextPeek;
    }
    // 무작위 재생
    /**
     *
     * @return {Boolean}
     */
    shuffleToggle() {
        return this._shuffle = !this._shuffle;
    }

    // 비공개 메소드

}

export default OAudioPlayer;