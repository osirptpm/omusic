class _Coordinates {
    constructor() {
        this.X = 0;
        this.Y = 0;
    }
    get Y() {
        return this._Y;
    }
    set Y(number) {
        if (number.constructor.name !== "Number") throw TypeError("Not a Number");
        this._Y = number;
    }
    get X() {
        return this._X;
    }
    set X(number) {
        if (number.constructor.name !== "Number") throw TypeError("Not a Number");
        this._X = number;
    }
}

class _EVOption {
    constructor() {
        this.centerPoint = new _Coordinates();
        this.color = "rgba(0, 0, 0, 1)";
        this.linear = true;
        this.bar = true;
        this.amount = 1024;
        this.clockwise = true;
        this.lineWidth = 1;
        this.insideRadius = 100;
        this.outsideRadius = 150;
        this.barLength = this.outsideRadius - this.insideRadius;
        this.startDegree = 0;
        this.capInterval = 5;
    }
    get centerPoint() {
        return this._centerPoint;
    }
    set centerPoint(coordinates) {
        if (coordinates.constructor.name !== "_Coordinates") throw TypeError("Not a _Coordinates");
        this._centerPoint = coordinates;
    }
    get color() {
        return this._color;
    }
    set color(string) {
        if (string.constructor.name !== "String") throw TypeError("Not a String");
        this._color = string;
    }
    get linear() {
        return this._linear;
    }
    set linear(boolean) {
        if (boolean.constructor.name !== "Boolean") throw TypeError("Not a Boolean");
        this._linear = boolean;
    }
    get bar() {
        return this._bar;
    }
    set bar(boolean) {
        if (boolean.constructor.name !== "Boolean") throw TypeError("Not a Boolean");
        this._bar = boolean;
    }
    get amount() {
        return this._amount;
    }
    set amount(number) {
        if (number.constructor.name !== "Number") throw TypeError("Not a Number");
        this._amount = number;
    }
    get clockwise() {
        return this._clockwise;
    }
    set clockwise(boolean) {
        if (boolean.constructor.name !== "Boolean") throw TypeError("Not a Boolean");
        this._clockwise = boolean ? 1 : -1;
    }
    get lineWidth() {
        return this._lineWidth;
    }
    set lineWidth(number) {
        if (number.constructor.name !== "Number") throw TypeError("Not a Number");
        this._lineWidth = number;
    }
    get outsideRadius() {
        return this._outsideRadius;
    }
    set outsideRadius(number) {
        if (number.constructor.name !== "Number") throw TypeError("Not a Number");
        this._outsideRadius = number;
        this.barLength = this.outsideRadius - this.insideRadius;
    }
    get insideRadius() {
        return this._insideRadius;
    }
    set insideRadius(number) {
        if (number.constructor.name !== "Number") throw TypeError("Not a Number");
        this._insideRadius = number;
        this.barLength = this.outsideRadius - this.insideRadius;
    }
    get barLength() {
        return this._barLength;
    }
    set barLength(number) {
        if (number.constructor.name !== "Number") throw TypeError("Not a Number");
        this._barLength = number;
    }
    get startDegree() {
        return this._startDegree;
    }
    set startDegree(number) {
        if (number.constructor.name !== "Number") throw TypeError("Not a Number");
        this._startDegree = number;
    }
    get capInterval() {
        return this._capInterval;
    }
    set capInterval(number) {
        if (number.constructor.name !== "Number") throw TypeError("Not a Number");
        this._capInterval = number;
    }
}

class OEqualizerVisualizer {
    constructor() {
    }
    // 공개 속성

    // 공개 메소드
    /**
     *
     * @returns {_EVOption}
     */
    static createEVOption() {
        return new _EVOption();
    }
    /**
     *
     * @param {CanvasRenderingContext2D} canvasContext
     * @param {Uint8Array} analyzedData
     * @param {_EVOption} evOption
     */
    static drawCircle(canvasContext, analyzedData, evOption) {
        if (canvasContext.constructor.name !== "CanvasRenderingContext2D") throw TypeError("Not a CanvasRenderingContext2D");

        let x1, y1, x2, y2, x3, y3, x4, y4;
        canvasContext.clearRect(0, 0, canvasContext.canvas.width, canvasContext.canvas.height);
        for (let i = 0; i < evOption.amount; i++) {
            canvasContext.lineWidth = evOption.lineWidth;
            canvasContext.strokeStyle = evOption.color;
            canvasContext.lineCap = "butt";
            canvasContext.beginPath();
            let valueRadius = evOption.insideRadius + analyzedData[i] / 255 * evOption.barLength;
            let circleExpressionX = Math.cos((360 / evOption.amount * i + evOption.startDegree) * Math.PI / 180);
            let circleExpressionY = evOption.clockwise * Math.sin((360 / evOption.amount * i + evOption.startDegree) * Math.PI / 180);
            if (evOption.bar) {
                x1 = circleExpressionX * evOption.insideRadius + evOption.centerPoint.X;
                y1 = circleExpressionY * evOption.insideRadius + evOption.centerPoint.Y;
                x2 = circleExpressionX * valueRadius + evOption.centerPoint.X;
                y2 = circleExpressionY * valueRadius + evOption.centerPoint.Y;
                canvasContext.moveTo(x1, y1);
                canvasContext.lineTo(x2, y2);
            }
            canvasContext.stroke();

            canvasContext.lineWidth = 1;
            canvasContext.beginPath();
            if (evOption.linear) {
                valueRadius += evOption.capInterval;
                if (i === 0) {
                    x4 = x3 = circleExpressionX * valueRadius + evOption.centerPoint.X;
                    y4 = y3 = circleExpressionY * valueRadius + evOption.centerPoint.Y;
                } else {
                    canvasContext.moveTo(x3, y3);
                    x3 = circleExpressionX * valueRadius + evOption.centerPoint.X;
                    y3 = circleExpressionY * valueRadius + evOption.centerPoint.Y;
                    canvasContext.lineTo(x3, y3);
                }
            }
            canvasContext.stroke();
        }
        canvasContext.beginPath();
        canvasContext.moveTo(x3, y3);
        canvasContext.lineTo(x4, y4);
        canvasContext.stroke();
    }
}

class OEqualizer extends AudioContext {
    constructor(audio) {
        super();
        this._source = this.createMediaElementSource(audio);
        this._analyser = this.createAnalyser();
        this._source.connect(this._analyser);
        this._analyser.connect(this.destination);
        this._analyzedData = new Uint8Array(this.__analyser.frequencyBinCount);
    }

    // 비공개 속성
    /**
     *
     * @param {MediaElementAudioSourceNode} mediaElementSource
     * @private
     */
    set _source(mediaElementSource) {
        this.__source = mediaElementSource;
    }
    /**
     *
     * @return {MediaElementAudioSourceNode}
     * @private
     */
    get _source() {
        return this.__source;
    }

    /**
     *
     * @param {AnalyserNode} analyser
     * @private
     */
    set _analyser(analyser) {
        this.__analyser = analyser;
    }
    /**
     *
     * @return {AnalyserNode}
     * @private
     */
    get _analyser() {
        return this.__analyser;
    }

    /**
     *
     * @param {Uint8Array} analyzedData
     * @private
     */
    set _analyzedData(analyzedData) {
        this.__analyzedData = analyzedData;
    }
    /**
     *
     * @return {Uint8Array}
     * @private
     */
    get _analyzedData() {
        return this.__analyzedData;
    }

    // 공개 속성
    /**
     *
     * @return {Uint8Array} analyzedData
     */
    get getAnalyzedByteFrequencyData() {
        this._analyser.getByteFrequencyData(this._analyzedData);
        return this._analyzedData;
    }
    /**
     *
     * @return {Uint8Array} analyzedData
     */
    get getAnalyzedByteTimeDomainData() {
        this._analyser.getByteTimeDomainData(this._analyzedData);
        return this._analyzedData;
    }


}

export { OEqualizer, OEqualizerVisualizer };