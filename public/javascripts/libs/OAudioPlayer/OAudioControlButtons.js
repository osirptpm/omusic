import {Coordinate, Line, LineAnimationInfo, OCanvasAnimation} from "../OCanvasAnimation/OCanvasAnimation.js";

class _ButtonObject {
    constructor(canvasRenderingContext2D) {
        if(canvasRenderingContext2D.constructor.name !== "CanvasRenderingContext2D") throw TypeError("Not a CanvasRenderingContext2D");
        this.context = canvasRenderingContext2D;
        //this.context.canvas.width = this.context.canvas.scrollWidth;
        //this.context.canvas.height = this.context.canvas.scrollHeight;
    }
    /**
     *
     * @return {CanvasRenderingContext2D}
     */
    get context() {
        return this._context;
    }
    /**
     *
     * @param {CanvasRenderingContext2D} canvasRenderingContext2D
     */
    set context(canvasRenderingContext2D) {
        if(canvasRenderingContext2D.constructor.name !== "CanvasRenderingContext2D") throw TypeError("Not a CanvasRenderingContext2D");
        this._context = canvasRenderingContext2D;
    }

}

class PreviousButton extends _ButtonObject {
    constructor(canvasRenderingContext2D) {
        super(canvasRenderingContext2D);
        this.line1 = new Line();
        this.line1.head = new Coordinate(30, 25);
        this.line1.tail = new Coordinate(30, 75);
        this.line2 = new Line();
        this.line2.head = new Coordinate(70, 25);
        this.line2.tail = new Coordinate(40, 50);
        this.line3 = new Line();
        this.line3.head = new Coordinate(40, 50);
        this.line3.tail = new Coordinate(70, 75);
        this._drawButton(this.context);
    }

    // 공개 속성

    /**
     *
     * @return {Line}
     */
    get line1() {
        return this._line1;
    }
    /**
     *
     * @param {Line} _line
     */
    set line1(_line) {
        if (_line.constructor.name !== "Line") throw TypeError("Not a Line");
        this._line1 = _line;
    }

    /**
     *
     * @return {Line}
     */
    get line2() {
        return this._line2;
    }
    /**
     *
     * @param {Line} _line
     */
    set line2(_line) {
        if (_line.constructor.name !== "Line") throw TypeError("Not a Line");
        this._line2 = _line;
    }

    /**
     *
     * @return {Line}
     */
    get line3() {
        return this._line3;
    }
    /**
     *
     * @param {Line} _line
     */
    set line3(_line) {
        if (_line.constructor.name !== "Line") throw TypeError("Not a Line");
        this._line3 = _line;
    }

    // 비공개 메소드

    /**
     *
     * @param {CanvasRenderingContext2D} ctx
     * @private
     */
    _drawButton(ctx) {
        ctx.beginPath();
        OCanvasAnimation.drawLine(ctx, this.line1);
        OCanvasAnimation.drawLine(ctx, this.line2);
        OCanvasAnimation.drawLine(ctx, this.line3);
        ctx.closePath();
        ctx.stroke();
    }
}
class PPButton extends _ButtonObject {
    constructor(canvasRenderingContext2D) {
        super(canvasRenderingContext2D);
        this.line1 = new Line();
        this.line1.head = new Coordinate(38, 25);
        this.line1.tail = new Coordinate(68, 50);
        this.line2 = new Line();
        this.line2.head = new Coordinate(68, 50);
        this.line2.tail = new Coordinate(38, 75);
        this._drawButton(this.context);

        this._oCanvasAnimation = new OCanvasAnimation(this.context);

        let line1AnimationInfo = new LineAnimationInfo();
        line1AnimationInfo.context = this.context;
        line1AnimationInfo.startMilliseconds = 0;
        line1AnimationInfo.endMilliseconds = 300;
        line1AnimationInfo.fps = 60;
        this._line1AnimationInfo = line1AnimationInfo;

        let line2AnimationInfo = new LineAnimationInfo();
        line2AnimationInfo.context = this.context;
        line2AnimationInfo.startMilliseconds = 0;
        line2AnimationInfo.endMilliseconds = 300;
        line2AnimationInfo.fps = 60;
        this._line2AnimationInfo = line2AnimationInfo;

    }

    /**
     *
     * @return {OCanvasAnimation}
     * @private
     */
    get _oCanvasAnimation() {
        return this.__oCanvasAnimation;
    }
    /**
     *
     * @param {OCanvasAnimation} oCanvasAnimation
     * @private
     */
    set _oCanvasAnimation(oCanvasAnimation) {
        if (oCanvasAnimation.constructor.name !== "OCanvasAnimation") throw TypeError("Not a OCanvasAnimation");
        this.__oCanvasAnimation = oCanvasAnimation;
    }

    /**
     *
     * @return {LineAnimationInfo}
     * @private
     */
    get _line1AnimationInfo() {
        return this.__line1AnimationInfo;
    }
    /**
     *
     * @param {LineAnimationInfo} lineAnimationInfo
     * @private
     */
    set _line1AnimationInfo(lineAnimationInfo) {
        if (lineAnimationInfo.constructor.name !== "LineAnimationInfo") throw TypeError("Not a LineAnimationInfo");
        this.__line1AnimationInfo = lineAnimationInfo;
    }

    /**
     *
     * @return {LineAnimationInfo}
     * @private
     */
    get _line2AnimationInfo() {
        return this.__line2AnimationInfo;
    }
    /**
     *
     * @param {LineAnimationInfo} lineAnimationInfo
     * @private
     */
    set _line2AnimationInfo(lineAnimationInfo) {
        if (lineAnimationInfo.constructor.name !== "LineAnimationInfo") throw TypeError("Not a LineAnimationInfo");
        this.__line2AnimationInfo = lineAnimationInfo;
    }

    /**
     *
     * @return {Line}
     */
    get line1() {
        return this._line1;
    }
    /**
     *
     * @param {Line} _line
     */
    set line1(_line) {
        if (_line.constructor.name !== "Line") throw TypeError("Not a Line");
        this._line1 = _line;
    }

    /**
     *
     * @return {Line}
     */
    get line2() {
        return this._line2;
    }
    /**
     *
     * @param {Line} _line
     */
    set line2(_line) {
        if (_line.constructor.name !== "Line") throw TypeError("Not a Line");
        this._line2 = _line;
    }

    // 사용자 호출 메소드

    // 눌릴 때
    /**
     *
     * @param {Boolean} isPause
     */
    PP(isPause) {
        let line1AnimationInfo = this._line1AnimationInfo;
        let line2AnimationInfo = this._line2AnimationInfo;

        if (isPause) {
            let toLine1 = new Line();
            toLine1.head = new Coordinate(38, 25);
            toLine1.tail = new Coordinate(68, 50);
            line1AnimationInfo.from = this.line1;
            line1AnimationInfo.to = toLine1;

            let toLine2 = new Line();
            toLine2.head = new Coordinate(68, 50);
            toLine2.tail = new Coordinate(38, 75);
            line2AnimationInfo.from = this.line2;
            line2AnimationInfo.to = toLine2;

            this._oCanvasAnimation.stop([line1AnimationInfo, line2AnimationInfo]);
            this._oCanvasAnimation.start([line1AnimationInfo, line2AnimationInfo]);

            this.line1 = toLine1;
            this.line2 = toLine2;
        } else {
            let toLine1 = new Line();
            toLine1.head = new Coordinate(35, 70);
            toLine1.tail = new Coordinate(35, 30);
            line1AnimationInfo.from = this.line1;
            line1AnimationInfo.to = toLine1;

            let toLine2 = new Line();
            toLine2.head = new Coordinate(65, 30);
            toLine2.tail = new Coordinate(65, 70);
            line2AnimationInfo.from = this.line2;
            line2AnimationInfo.to = toLine2;

            this._oCanvasAnimation.stop([line1AnimationInfo, line2AnimationInfo]);
            this._oCanvasAnimation.start([line1AnimationInfo, line2AnimationInfo]);

            this.line1 = toLine1;
            this.line2 = toLine2;
        }
    }

    // 비공개 메소드

    /**
     *
     * @param {CanvasRenderingContext2D} ctx
     * @private
     */
    _drawButton(ctx) {
        ctx.beginPath();
        OCanvasAnimation.drawLine(ctx, this.line1);
        OCanvasAnimation.drawLine(ctx, this.line2);
        ctx.closePath();
        ctx.stroke();
    }
}
class NextButton extends _ButtonObject {
    constructor(canvasRenderingContext2D) {
        super(canvasRenderingContext2D);
        this.line1 = new Line();
        this.line1.head = new Coordinate(30, 25);
        this.line1.tail = new Coordinate(60, 50);
        this.line2 = new Line();
        this.line2.head = new Coordinate(60, 50);
        this.line2.tail = new Coordinate(30, 75);
        this.line3 = new Line();
        this.line3.head = new Coordinate(70, 25);
        this.line3.tail = new Coordinate(70, 75);
        this._drawButton(this.context);
    }

    // 공개 속성

    /**
     *
     * @return {Line}
     */
    get line1() {
        return this._line1;
    }
    /**
     *
     * @param {Line} _line
     */
    set line1(_line) {
        if (_line.constructor.name !== "Line") throw TypeError("Not a Line");
        this._line1 = _line;
    }

    /**
     *
     * @return {Line}
     */
    get line2() {
        return this._line2;
    }
    /**
     *
     * @param {Line} _line
     */
    set line2(_line) {
        if (_line.constructor.name !== "Line") throw TypeError("Not a Line");
        this._line2 = _line;
    }

    /**
     *
     * @return {Line}
     */
    get line3() {
        return this._line3;
    }
    /**
     *
     * @param {Line} _line
     */
    set line3(_line) {
        if (_line.constructor.name !== "Line") throw TypeError("Not a Line");
        this._line3 = _line;
    }

    // 비공개 메소드

    /**
     *
     * @param {CanvasRenderingContext2D} ctx
     * @private
     */
    _drawButton(ctx) {
        ctx.beginPath();
        OCanvasAnimation.drawLine(ctx, this.line1);
        OCanvasAnimation.drawLine(ctx, this.line2);
        OCanvasAnimation.drawLine(ctx, this.line3);
        ctx.closePath();
        ctx.stroke();
    }
}
export {PPButton, NextButton, PreviousButton};