class OElement {
    /**
     *
     * @param {Element} element
     * @param {string} path
     */
    constructor(element, path) {
        //const shadow = element.attachShadow({mode : "open"});
        this._loadFiles(path, element);
    }


    _loadFiles(path, element) {
        let filename = this._getFilename(path);
        path = path + "/" + filename;
        this._getCSS(path + ".css").then(css => {
            let pattern = new RegExp(filename, "g");
            console.log(pattern.exec(css));
            element.innerHTML += "<style>\n" +  css + "\n</style>";
        });
        this._getHTML(path + ".html").then(html => {
            element.innerHTML += html;

            this._getJS(path + ".js").then(js => {
                let script = document.createElement("script");
                script.innerHTML = "" +
                    "let shadow = document.getElementById(\"" + filename + 1 + "\");\n" +
                    "let " + filename + " = shadow.getElementById(\"" + filename + "\");\n" +
                    "\n" +
                    js;
                element.appendChild(script);
            });
        });
    }

    _getFile(path, type) {
        return new Promise((resolve, reject) => {
            let req = new XMLHttpRequest();
            req.responseType = type;
            req.onload = function() {
                if (req.status === 200) {
                    resolve(req.response);
                }
                else {
                    reject(Error(req.statusText));
                }
            };
            req.open("GET", path);
            req.send();
        });
    }

    _getHTML(path) {
        return this._getFile(path, "");
    }

    _getCSS(path) {
        return this._getFile(path, "");
    }

    _getJS(path) {
        return this._getFile(path, "");
    }

    _getFilename(path) {
        let dirname = path.split("/");
        return dirname[dirname.length - 1];
    }
}

export default OElement;