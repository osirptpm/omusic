class Coordinate {
    /**
     * {number} [x]
     * {number} [y]
     */
    constructor() {
        if (arguments.length === 2) {
            this.X = arguments[0];
            this.Y = arguments[1];
        }
    }
    /**
     *
     * @return {number}
     */
    get X() {
        return this._X;
    }
    /**
     *
     * @param {number} number
     */
    set X(number) {
        if (number.constructor.name !== "Number") throw TypeError("Not a Number");
        this._X = number;
    }
    /**
     *
     * @return {number}
     */
    get Y() {
        return this._Y;
    }
    /**
     *
     * @param {number} number
     */
    set Y(number) {
        if (number.constructor.name !== "Number") throw TypeError("Not a Number");
        this._Y = number;
    }
}

class _Shape {

}

class Line extends _Shape {
    constructor() {
        super();
        this.head = new Coordinate(0, 0);
        this.tail = new Coordinate(0, 0);
    }
    /**
     *
     * @return {Coordinate}
     */
    get head() {
        return this._head;
    }
    /**
     *
     * @param {Coordinate} coordinate
     */
    set head(coordinate) {
        if (coordinate.constructor.name !== "Coordinate") throw TypeError("Not a Coordinate");
        this._head = coordinate;
    }

    /**
     *
     * @return {Coordinate}
     */
    get tail() {
        return this._tail;
    }
    /**
     *
     * @param {Coordinate} coordinate
     */
    set tail(coordinate) {
        if (coordinate.constructor.name !== "Coordinate") throw TypeError("Not a Coordinate");
        this._tail = coordinate;
    }
}

class _AnimationInfo {

    // 비공개 속성
    /**
     *
     * @return {number}
     */
    get _setInterval() {
        return this.__setInterval;
    }
    /**
     *
     * @param {number} number
     */
    set _setInterval(number) {
        if (number.constructor.name !== "Number") throw TypeError("Not a Number");
        this.__setInterval = number;
    }

    // 공개 속성
    /**
     *
     * @return {CanvasRenderingContext2D}
     */
    get context() {
        return this._context;
    }
    /**
     *
     * @param {CanvasRenderingContext2D} canvasRenderingContext2D
     */
    set context(canvasRenderingContext2D) {
        if(canvasRenderingContext2D.constructor.name !== "CanvasRenderingContext2D") throw TypeError("Not a CanvasRenderingContext2D");
        this._context = canvasRenderingContext2D;
    }

    /**
     *
     * @return {number}
     */
    get startMilliseconds() {
        return this._startMilliseconds;
    }
    /**
     *
     * @param {number} number
     */
    set startMilliseconds(number) {
        if (number.constructor.name !== "Number") throw TypeError("Not a Number");
        this._startMilliseconds = number;
    }
    /**
     *
     * @return {number}
     */
    get endMilliseconds() {
        return this._endMilliseconds;
    }
    /**
     *
     * @param {number} number
     */
    set endMilliseconds(number) {
        if (number.constructor.name !== "Number") throw TypeError("Not a Number");
        this._endMilliseconds = number;
    }

    /**
     *
     * @return {number}
     */
    get fps() {
        return this._fps;
    }
    /**
     *
     * @param {number} number
     */
    set fps(number) {
        if (number.constructor.name !== "Number") throw TypeError("Not a Number");
        this._fps = number;
    }

    /**
     *
     * @return {_Shape}
     */
    get from() {
        return this._from;
    }
    /**
     *
     * @param {_Shape} _shape
     */
    set from(_shape) {
        if (_shape.constructor.__proto__.name !== "_Shape") throw TypeError("Not a _Shape");
        this._from = _shape;
    }

    /**
     *
     * @return {_Shape}
     */
    get to() {
        return this._to;
    }
    /**
     *
     * @param {_Shape} _shape
     */
    set to(_shape) {
        if (_shape.constructor.__proto__.name !== "_Shape") throw TypeError("Not a _Shape");
        this._to = _shape;
    }

    /**
     *
     * @return {_Shape}
     */
    get nowShape() {
        return this._nowShape;
    }
    /**
     *
     * @param {_Shape} _shape
     */
    set nowShape(_shape) {
        if (_shape.constructor.__proto__.name !== "_Shape") throw TypeError("Not a _Shape");
        this._nowShape = _shape;
    }

    start() {
    }
    stop() {
    }

}

class LineAnimationInfo extends _AnimationInfo {
    constructor() {
        super();
        this.nowShape = new Line();
    }

    // 비공개 속성

    // 공개 속성

    /**
     *
     * @return {Line}
     */
    get from() {
        return this._from;
    }
    /**
     *
     * @param {Line} line
     */
    set from(line) {
        if (line.constructor.name !== "Line") throw TypeError("Not a Line");
        this._from = line;
    }

    /**
     *
     * @return {Line}
     */
    get to() {
        return this._to;
    }
    /**
     *
     * @param {Line} line
     */
    set to(line) {
        if (line.constructor.name !== "Line") throw TypeError("Not a Line");
        this._to = line;
    }

    start() {
        let milliseconds = this.endMilliseconds - this.startMilliseconds;
        let fps = this.fps;

        let seconds = milliseconds / 1000;
        let totalFrames = seconds * fps;
        let interval = milliseconds / totalFrames;

        let headX = this._exp(this.from.head.X, this.to.head.X, totalFrames);
        let headY = this._exp(this.from.head.Y, this.to.head.Y, totalFrames);
        let tailX = this._exp(this.from.tail.X, this.to.tail.X, totalFrames);
        let tailY = this._exp(this.from.tail.Y, this.to.tail.Y, totalFrames);

        let t = 0;
        let line = this.nowShape;

        this._setInterval = setInterval(() => {

            line.head = new Coordinate(headX(t), headY(t));
            line.tail = new Coordinate(tailX(t), tailY(t));

            if (t >= totalFrames) {
                this.stop();
            }
            t++;
        }, interval);
    }
    stop() {
        clearInterval(this._setInterval);
    }


    /**
     *
     * @param {number} from
     * @param {number} to
     * @param {number} xMax
     * @return {function({number}): number}
     * @private
     */
    _exp(from, to, xMax) {
        return t => (to - from) / xMax * t + from;
    }
}

class OCanvasAnimation {
    /**
     *
     * @param {CanvasRenderingContext2D} canvasRenderingContext2D
     */
    constructor(canvasRenderingContext2D) {
        this._setInterval = -1;
        this.fps = 60;
        this.context = canvasRenderingContext2D;
    }


    // 비공개 속성

    /**
     *
     * @return {number}
     */
    get _setInterval() {
        return this.__setInterval;
    }
    /**
     *
     * @param {number} number
     */
    set _setInterval(number) {
        if (number.constructor.name !== "Number") throw TypeError("Not a Number");
        this.__setInterval = number;
    }

    // 공개 속성

    /**
     *
     * @return {CanvasRenderingContext2D}
     */
    get context() {
        return this._context;
    }
    /**
     *
     * @param {CanvasRenderingContext2D} canvasRenderingContext2D
     */
    set context(canvasRenderingContext2D) {
        if(canvasRenderingContext2D.constructor.name !== "CanvasRenderingContext2D") throw TypeError("Not a CanvasRenderingContext2D");
        this._context = canvasRenderingContext2D;
    }

    /**
     *
     * @return {number}
     */
    get fps() {
        return this._fps;
    }
    /**
     *
     * @param {number} number
     */
    set fps(number) {
        if (number.constructor.name !== "Number") throw TypeError("Not a Number");
        this._fps = number;
    }

    // 사용자 호출 메소드

    // 애니메이션 시작
    /**
     *
     * @param {Array.<_AnimationInfo>} _animationInfoArray
     */
    start(_animationInfoArray) {
        // 이미 실행 중이면 중지
        if (this._setInterval !== -1) this.stop(_animationInfoArray);
        let interval = 1000 / this.fps;

        // 모든 애니메이션이 끝나는 시간 구하기
        let endMilliseconds = this._getEndMilliseconds(_animationInfoArray);
        let _shapeArray = this._getShapes(_animationInfoArray);

        let beginMilliseconds = performance.now();
        endMilliseconds = endMilliseconds + beginMilliseconds + 100;

        this._setInterval = setInterval(() => {
            let nowMilliseconds = performance.now();
            _animationInfoArray = _animationInfoArray.filter(_animationInfo => {
                // 해당 객체의 애니메이션 시작 시간이 되면 시작
                if (this._isTimesUp(_animationInfo.startMilliseconds + beginMilliseconds, nowMilliseconds)) {
                    _animationInfo.start();
                    return false;
                }
                return true;
            });
            OCanvasAnimation.drawShapes(this.context, _shapeArray);

            // 모든 애니메이션이 끝날 시간이 되면 중지
            if (this._isTimesUp(endMilliseconds, nowMilliseconds)) this.stop();
        }, interval);
    }

    // 애니메이션 멈춤
    stop(_animationInfoArray) {
        if (_animationInfoArray !== undefined) {
            _animationInfoArray.forEach(_animationInfo => {
                _animationInfo.stop();
            });
        }
        clearInterval(this._setInterval);
        this._setInterval = -1;
    }

    // 오브젝트들 그리기
    /**
     *
     * @param {CanvasRenderingContext2D} canvasRenderingContext2D
     * @param {Array.<_Shape>} shapeArray
     */
    static drawShapes(canvasRenderingContext2D, shapeArray) { // todo 일반화 하는 중 // 불안정
        let width = canvasRenderingContext2D.canvas.width, height = canvasRenderingContext2D.canvas.height;
        canvasRenderingContext2D.clearRect(0, 0, width, height);
        canvasRenderingContext2D.beginPath();
        shapeArray.forEach(_shape => {
            switch (_shape.constructor.name) {
                case Line.name: OCanvasAnimation.drawLine(canvasRenderingContext2D, _shape); break;
            }
        });
        canvasRenderingContext2D.closePath();
        canvasRenderingContext2D.stroke();
    }

    // 선들 그리기
    /**
     *
     * @param {CanvasRenderingContext2D} canvasRenderingContext2D
     * @param {Array.<Line>} lineArray
     */
    static drawLines(canvasRenderingContext2D, lineArray) {
        let width = canvasRenderingContext2D.canvas.width, height = canvasRenderingContext2D.canvas.height;
        canvasRenderingContext2D.clearRect(0, 0, width, height);
        canvasRenderingContext2D.beginPath();
        lineArray.forEach(line => {
            OCanvasAnimation.drawLine(canvasRenderingContext2D, line);
        });
        canvasRenderingContext2D.closePath();
        canvasRenderingContext2D.stroke();
    }

    // 선 그리기
    /**
     *
     * @param {CanvasRenderingContext2D} canvasRenderingContext2D
     * @param {Line} line
     */
    static drawLine(canvasRenderingContext2D, line) {
        let width = canvasRenderingContext2D.canvas.width, height = canvasRenderingContext2D.canvas.height;
        canvasRenderingContext2D.moveTo(OCanvasAnimation._scale(line.head.X, width), OCanvasAnimation._scale(line.head.Y, height));
        canvasRenderingContext2D.lineTo(OCanvasAnimation._scale(line.tail.X, width), OCanvasAnimation._scale(line.tail.Y, height));
    }

    // 비공개 메소드

    // 모든 애니메이션 끝나는 시간 구하기
    /**
     *
     * @param {Array.<_AnimationInfo>} _animationInfoArray
     * @return {number}
     * @private
     */
    _getEndMilliseconds(_animationInfoArray) {
        let endMilliseconds = 0;
        _animationInfoArray.forEach(animationInfo => {
            if (animationInfo.endMilliseconds > endMilliseconds) endMilliseconds = animationInfo.endMilliseconds;
        });
        return endMilliseconds;
    }

    // _ShapeArray 리턴
    /**
     *
     * @param {Array.<_AnimationInfo>} _animationInfoArray
     * @return {Array.<_Shape>}
     * @private
     */
    _getShapes(_animationInfoArray) {
        let shapeArray = [];
        _animationInfoArray.forEach(animationInfo => {
            shapeArray.push(animationInfo.nowShape);
        });
        return shapeArray;
    }

    // 시간 체크
    /**
     *
     * @param {number} milliseconds
     * @param {number} nowMilliseconds
     * @return {Boolean}
     * @private
     */
    _isTimesUp(milliseconds, nowMilliseconds) {
        return milliseconds <= nowMilliseconds;
    }

    /**
     *
     * @param {number} number
     * @param {number} xAxisLength
     * @return {number}
     * @private
     */
    static _scale(number, xAxisLength) {
        return number / 100 * xAxisLength;
    }
}

export {OCanvasAnimation, Coordinate, LineAnimationInfo, Line};