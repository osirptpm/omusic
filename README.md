# OMusic javascript lib

- Web Audio API를 사용한 Visualizer
- audio 파일 태그 추출기
    - mp3 (id3v1, id3v2), VorbisComment(flac, ogg)
    - albumArt 추출
- 이미지 블러 처리기


![play screen](public/images/play-screen.jpg)
![play screen](public/images/tag-screen.jpg)